const Sequelize = require('sequelize');
const sequelize = require('./index');

const Question = require('./question.model');

const TofQuestion = sequelize.define('tof_question', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    question: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    formulation: {
        type: Sequelize.TEXT,
        allowNull: false
    },
    correct_tof: {
        type: Sequelize.STRING(1),
        allowNull: false
    },
    correct_justification: {
        type: Sequelize.TEXT,
    }
});

TofQuestion.belongsTo(Question, {
    foreignKey: 'question',
    as: 'question_id',
    targetKey: 'id',
    onUpdate: 'CASCADE',
    onDelete: 'NO ACTION'

});

module.exports = TofQuestion;