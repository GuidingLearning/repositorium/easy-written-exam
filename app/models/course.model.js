const Sequelize = require('sequelize');
const sequelize = require('./index');

const Course = sequelize.define('course', {
    id: {
        type: Sequelize.STRING(40),
        primaryKey: true
    },
    name: {
        type: Sequelize.STRING(150),
        allowNull: false
    },
    university: {
        type: Sequelize.STRING(150),
        allowNull: false
    },
    code: {
        type: Sequelize.STRING(30),
        allowNull: false
    },
    semester: {
        type: Sequelize.STRING(50),
        allowNull: false
    },
    year: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    professors: {
        type: Sequelize.ARRAY(Sequelize.INTEGER)
    },
    evaluators: {
        type: Sequelize.ARRAY(Sequelize.INTEGER)
    },
    students: {
        type: Sequelize.ARRAY(Sequelize.INTEGER)
    }
});

module.exports = Course;