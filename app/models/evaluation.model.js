const Sequelize = require('sequelize');
const sequelize = require('./index');

const Answer = require('./answer.model');
const User = require('./user.model');

const Evaluation = sequelize.define('evaluation', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
    },
    answer: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    grade: {
        type: Sequelize.REAL,
        allowNull: false
    },
    feedback: {
        type: Sequelize.TEXT
    },
    evaluator: {
        type: Sequelize.INTEGER,
        allowNull: false
    }
});

Evaluation.belongsTo(Answer, {
    foreignKey: 'answer',
    as: 'answer_id',
    targetKey: 'id',
    onUpdate: 'CASCADE',
    onDelete: 'NO ACTION'

});

Evaluation.belongsTo(User, {
    foreignKey: 'evaluator',
    as: 'evaluator_id',
    targetKey: 'id',
    onUpdate: 'CASCADE',
    onDelete: 'NO ACTION'

});

module.exports = Evaluation;