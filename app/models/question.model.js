const Sequelize = require('sequelize');
const sequelize = require('./index');

const Course = require('./course.model');
const User = require('./user.model');
const QuestionType = require('./question-type.model');

const Question = sequelize.define('question', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    course: {
        type: Sequelize.STRING(40),
        allowNull: false
    },
    creator: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    title: {
        type: Sequelize.STRING(50),
        allowNull: false
    },
    creation_date: {
        type: Sequelize.DATE,
        allowNull: false
    },
    category: {
        type: Sequelize.STRING(50),
        allowNull: false
    },
    question_type: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    tags: {
        type: Sequelize.ARRAY(Sequelize.STRING(30)),
    }
});

Question.belongsTo(Course, {
    foreignKey: 'course',
    as: 'course_id',
    targetKey: 'id',
    onUpdate: 'CASCADE',
    onDelete: 'NO ACTION'

});

Question.belongsTo(User, {
    foreignKey: 'creator',
    as: 'creator_id',
    targetKey: 'id',
    onUpdate: 'CASCADE',
    onDelete: 'NO ACTION'

});

Question.belongsTo(QuestionType, {
    foreignKey: 'question_type',
    as: 'question_type_id',
    targetKey: 'id',
    onUpdate: 'CASCADE',
    onDelete: 'NO ACTION'

});

module.exports = Question;