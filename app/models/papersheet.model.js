const Sequelize = require('sequelize');
const sequelize = require('./index');

const Test = require('./test.model');
const User = require('./user.model');

const PaperSheet = sequelize.define('papersheet', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
    },
    test: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    student: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    start_date: {
        type: Sequelize.DATE,
        allowNull: false
    },
    submission_date: {
        type: Sequelize.DATE
    }
});

PaperSheet.belongsTo(User, {
    foreignKey: 'student',
    as: 'student_id',
    targetKey: 'id',
    onUpdate: 'CASCADE',
    onDelete: 'NO ACTION'

});

PaperSheet.belongsTo(Test, {
    foreignKey: 'test',
    as: 'test_id',
    targetKey: 'id',
    onUpdate: 'CASCADE',
    onDelete: 'NO ACTION'

});

module.exports = PaperSheet;