const Sequelize = require('sequelize');
const sequelize = require('./index');

const Question = require('./question.model');

const McQuestion = sequelize.define('mc_question', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    question: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    formulation: {
        type: Sequelize.TEXT,
        allowNull: false
    },
    images: {
        type: Sequelize.ARRAY(Sequelize.TEXT),
        allowNull: false
    },
    alternatives: {
        type: Sequelize.ARRAY(Sequelize.STRING(530)),
        allowNull: false
    },
    correct_alternative: {
        type: Sequelize.STRING(1),
        allowNull: false
    }
});

McQuestion.belongsTo(Question, {
    foreignKey: 'question',
    as: 'question_id',
    targetKey: 'id',
    onUpdate: 'CASCADE',
    onDelete: 'NO ACTION'

});

module.exports = McQuestion;