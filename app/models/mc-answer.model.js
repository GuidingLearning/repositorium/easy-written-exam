const Sequelize = require('sequelize');
const sequelize = require('./index');

const Answer = require('./answer.model');

const McAnswer = sequelize.define('mc_answer', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    answer_id: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    alternative: {
        type: Sequelize.STRING(1),
        allowNull: false
    }
});

McAnswer.belongsTo(Answer, {
    foreignKey: 'answer_id',
    as: 'answer',
    targetKey: 'id',
    onUpdate: 'CASCADE',
    onDelete: 'NO ACTION'

});

module.exports = McAnswer;