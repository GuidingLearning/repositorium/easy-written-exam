const Sequelize = require('sequelize');
const sequelize = require('./index');

const QuestionType = sequelize.define('question_type', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    type_name: {
        type: Sequelize.STRING(30),
        allowNull: false
    },
    description: {
        type: Sequelize.STRING(200),
        allowNull: false,
    }
});

module.exports = QuestionType;