const Sequelize = require('sequelize');
const sequelize = require('./index');

const RoleEwe = sequelize.define('ewe_role', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    name: {
        type: Sequelize.STRING(20),
        allowNull: false
    },
    description: {
        type: Sequelize.STRING,

    }
});

module.exports = RoleEwe;