const Sequelize = require('sequelize');
const sequelize = require('./index');

const Question = require('./question.model');

const Feedback = sequelize.define('feedback', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    question: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    tags: {
        type: Sequelize.ARRAY(Sequelize.STRING(30)),
        allowNull: false
    },
    text: {
        type: Sequelize.TEXT,
        allowNull: false
    }
});

Feedback.belongsTo(Question, {
    foreignKey: 'question',
    as: 'question_id',
    targetKey: 'id',
    onUpdate: 'CASCADE',
    onDelete: 'NO ACTION'

});

module.exports = Feedback;