const Sequelize = require('sequelize');
const sequelize = require('./index');

const Answer = require('./answer.model');

const EssayAnswer = sequelize.define('essay_answer', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    answer_id: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    answer_text: {
        type: Sequelize.TEXT,
        allowNull: false
    }
});

EssayAnswer.belongsTo(Answer, {
    foreignKey: 'answer_id',
    as: 'answer',
    targetKey: 'id',
    onUpdate: 'CASCADE',
    onDelete: 'NO ACTION'

});

module.exports = EssayAnswer;