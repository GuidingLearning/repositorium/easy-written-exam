const Sequelize = require('sequelize');
const sequelize = require('./index');

const Course = require('./course.model');
const User = require('./user.model');

const Test = sequelize.define('test', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    title: {
        type: Sequelize.STRING(100),
        allowNull: false
    },
    creator: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    course: {
        type: Sequelize.STRING(40),
        allowNull: false
    },
    instructions: {
        type: Sequelize.TEXT,
    },
    creation_date: {
        type: Sequelize.DATE,
        allowNull: false
    },
    start_date: {
        type: Sequelize.DATE,
        allowNull: false
    },
    deadline_date: {
        type: Sequelize.DATE,
        allowNull: false
    },
    duration: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    question_list: {
        type: Sequelize.ARRAY(Sequelize.INTEGER),
    }
});

Test.belongsTo(User, {
    foreignKey: 'creator',
    as: 'creator_id',
    targetKey: 'id',
    onUpdate: 'CASCADE',
    onDelete: 'NO ACTION'

});

Test.belongsTo(Course, {
    foreignKey: 'course',
    as: 'course_id',
    targetKey: 'id',
    onUpdate: 'CASCADE',
    onDelete: 'NO ACTION'

});

module.exports = Test;