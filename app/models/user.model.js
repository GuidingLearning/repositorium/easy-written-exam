const Sequelize = require('sequelize');
const sequelize = require('./index');

const RoleEwe = require('./role-ewe.model');

const User = sequelize.define('user', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    username: {
        type: Sequelize.STRING(20),
        allowNull: false
    },
    firstname: {
        type: Sequelize.STRING(50),
        allowNull: false
    },
    lastname: {
        type: Sequelize.STRING(50),
        allowNull: false
    },
    birthdate: {
        type: Sequelize.DATEONLY
    },
    email: {
        type: Sequelize.STRING(100),
        allowNull: false
    },
    password: {
        type: Sequelize.STRING,
        allowNull: false
    },
    ewe_role: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    inscriptions: {
        type: Sequelize.ARRAY(Sequelize.STRING(80))
    }
});

User.belongsTo(RoleEwe, {
    foreignKey: 'ewe_role',
    as: 'ewe_role_id',
    targetKey: 'id',
    onUpdate: 'CASCADE',
    onDelete: 'SET NULL'

});

module.exports = User;