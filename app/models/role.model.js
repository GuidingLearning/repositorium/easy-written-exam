const Sequelize = require('sequelize');
const sequelize = require('./index');

const Role = sequelize.define('role', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    name: {
        type: Sequelize.STRING(20),
        allowNull: false
    },
    description: {
        type: Sequelize.STRING,

    }
});

module.exports = Role;