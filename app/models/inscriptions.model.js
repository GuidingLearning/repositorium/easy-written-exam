const Sequelize = require('sequelize');
const sequelize = require('./index');

const Course = require('./course.model');
const User = require('./user.model');
const Role = require('./role.model');

const Inscription = sequelize.define('inscription', {
    id: {
        type: Sequelize.STRING(80),
        primaryKey: true,
    },
    course: {
        type: Sequelize.STRING(40),
        allowNull: false
    },
    user: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    role: {
        type: Sequelize.INTEGER,
        allowNull: false
    }
});

Inscription.belongsTo(Course, {
    foreignKey: 'course',
    as: 'course_id',
    targetKey: 'id',
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE'

});

Inscription.belongsTo(User, {
    foreignKey: 'user',
    as: 'user_id',
    targetKey: 'id',
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE'

});

Inscription.belongsTo(Role, {
    foreignKey: 'role',
    as: 'role_id',
    targetKey: 'id',
    onUpdate: 'CASCADE',
    onDelete: 'NO ACTION'

});

module.exports = Inscription;