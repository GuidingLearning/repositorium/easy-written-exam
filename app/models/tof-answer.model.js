const Sequelize = require('sequelize');
const sequelize = require('./index');

const Answer = require('./answer.model');

const TofAnswer = sequelize.define('tof_answer', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    answer_id: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    tof: {
        type: Sequelize.STRING(1),
        allowNull: false
    },
    justification: {
        type: Sequelize.TEXT
    }
});

TofAnswer.belongsTo(Answer, {
    foreignKey: 'answer_id',
    as: 'answer',
    targetKey: 'id',
    onUpdate: 'CASCADE',
    onDelete: 'NO ACTION'

});

module.exports = TofAnswer;