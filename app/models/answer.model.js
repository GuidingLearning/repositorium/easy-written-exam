const Sequelize = require('sequelize');
const sequelize = require('./index');

const PaperSheet = require('./papersheet.model');
const QuestionType = require('./question-type.model');
const User = require('./user.model');

const Answer = sequelize.define('answer', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true
    },
    papersheet: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    student: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    question_number: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    question_type: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    submission_time: {
        type: Sequelize.DATE,
        allowNull: false
    }
});

Answer.belongsTo(PaperSheet, {
    foreignKey: 'papersheet',
    as: 'papersheet_id',
    targetKey: 'id',
    onUpdate: 'CASCADE',
    onDelete: 'NO ACTION'

});

Answer.belongsTo(User, {
    foreignKey: 'student',
    as: 'student_id',
    targetKey: 'id',
    onUpdate: 'CASCADE',
    onDelete: 'NO ACTION'

});

Answer.belongsTo(QuestionType, {
    foreignKey: 'question_type',
    as: 'question_type_id',
    targetKey: 'id',
    onUpdate: 'CASCADE',
    onDelete: 'NO ACTION'

});

module.exports = Answer;