const express = require('express');
const router = express.Router();
const {authJwt} = require('../middleware');
const multer = require('multer');
const fs = require('fs');

const {
    getUserTestsFromThisYear,
    getTestImg,
    getTestById,
    createTest
} = require('../controllers/test.controller');


const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        let course = req.body.course;
        let title = req.body.test_title;
        let [q, _] = file.fieldname.split('_');
        let dir = `app/media/${course}/${title}/${q}`;
        if (!fs.existsSync(dir)){
            fs.mkdirSync(dir, { recursive: true });
        }
        cb(null, dir);
    },
    filename: function (req, file, cb) {
        let [_, i] = file.fieldname.split('_');
        let dir = `${i}-${Date.now()}-${file.originalname}`;
        cb(null, dir);
    }
});

const upload = multer({storage});


router.use(function (req, res, next) {
    res.header(
        'Access-Control-Allow-Headers',
        'x-access-token, Origin, Content-Type, Accept'
    );
    next();
});

router.get('/mine/home', [authJwt.verifyToken], getUserTestsFromThisYear);
router.get('/testimg/:imgEncodedPath', getTestImg);
router.get('/testid/:id', [authJwt.verifyToken], getTestById);

router.post('/new', [authJwt.verifyToken, upload.any()], createTest)

module.exports = router;