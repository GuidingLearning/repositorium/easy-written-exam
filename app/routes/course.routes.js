const express = require('express');
const router = express.Router();
const {authJwt} = require('../middleware');

const {
    addNewMember,
    getCourses,
    getCourseById,
    getCoursesByUniversity,
    getUserCourses,
    getMyCourses,
    getMyCoursesAsTeacher,
    joinCourse,
    createCourse,
} = require('../controllers/course.controller');


router.use(function (req, res, next) {
    res.header(
        'Access-Control-Allow-Headers',
        'x-access-token, Origin, Content-Type, Accept'
    );
    next();
});

router.get('/', getCourses);

router.get('/by-id/:courseId', [authJwt.verifyToken], getCourseById)

router.get('/university/:university', getCoursesByUniversity);

router.get('/userid/:id', getUserCourses);

router.get('/mine', [authJwt.verifyToken], getMyCourses);

router.get('/mine/for-test-design', [authJwt.verifyToken], getMyCoursesAsTeacher);

router.post('/add-member/', [authJwt.verifyToken], addNewMember)

router.post('/join', [authJwt.verifyToken], joinCourse);

router.post('/new', [authJwt.verifyToken], createCourse);

module.exports = router;