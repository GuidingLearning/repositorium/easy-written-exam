const express = require('express');
const router = express.Router();
const {verifySignUp} = require('../middleware');
const {
    signIn,
    signUp,
    getRoles,
    createRole,
    getRoleById,
    deleteRole,
    updateRole
} = require('../controllers/auth.controller');


router.use(function (req, res, next) {
    res.header(
        'Access-Control-Allow-Headers',
        'x-access-token, Origin, Content-Type, Accept'
    );
    next();
});

router.post(
    '/signup',
    [
        verifySignUp.checkDuplicateUsernameOrEmail,
        verifySignUp.checkRolesExisted,
        verifySignUp.checkPassword
    ],
    signUp
);

router.post('/signin', signIn);
router.post('/', createRole);
router.get('/', getRoles);
router.get('/role/:id', getRoleById);
router.delete('/delete/:id', deleteRole);
router.put('/update/:id', updateRole);

module.exports = router;