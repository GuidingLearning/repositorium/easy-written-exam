const express = require('express');
const router = express.Router();
const {authJwt} = require('../middleware');

const {
    allAccess,
    userBoard,
    moderatorBoard,
    adminBoard,
    getMyInfo,
    getUsers,
    createUser,
    getUserById,
    deleteUser,
    updateUser
} = require('../controllers/user.controller');


router.use(function (req, res, next) {
    res.header(
        'Access-Control-Allow-Headers',
        'x-access-token, Origin, Content-Type, Accept'
    );
    next();
});

router.get('/test/all', allAccess);

router.get(
    '/test/user',
    [authJwt.verifyToken],
    userBoard
);

router.get(
    '/test/mod',
    [authJwt.verifyToken, authJwt.isModerator],
    moderatorBoard
);

router.get(
    '/test/admin',
    [authJwt.verifyToken, authJwt.isAdmin],
    adminBoard
);

router.get(
    '/my-info',
    [authJwt.verifyToken],
    getMyInfo
);

router.post('/', createUser);
router.get('/', getUsers);
router.get('/user/:id', getUserById);
router.delete('/delete/:id', deleteUser);
router.put('/update/:id', updateUser);

module.exports = router;