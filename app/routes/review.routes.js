const express = require('express');
const router = express.Router();
const {authJwt} = require('../middleware');

const {
    getMyAnswers
} = require('../controllers/review.controller');


router.use(function (req, res, next) {
    res.header(
        'Access-Control-Allow-Headers',
        'x-access-token, Origin, Content-Type, Accept'
    );
    next();
});

router.get('/answers/:testId', [authJwt.verifyToken], getMyAnswers);

module.exports = router;