const express = require('express');
const router = express.Router();
const {authJwt} = require('../middleware');

const {
    getFeedback,
    getPaperSheetByTest,
    createEvaluation,
    evaluateMcAnswers
} = require('../controllers/evaluation.controller');


router.use(function (req, res, next) {
    res.header(
        'Access-Control-Allow-Headers',
        'x-access-token, Origin, Content-Type, Accept'
    );
    next();
});

router.get('/feedback/:question', [authJwt.verifyToken], getFeedback);
router.get('/papersheet/:testId', [authJwt.verifyToken], getPaperSheetByTest);
router.post('/create', [authJwt.verifyToken], createEvaluation);
router.post('/create/mc-answers', [authJwt.verifyToken], evaluateMcAnswers);

module.exports = router;