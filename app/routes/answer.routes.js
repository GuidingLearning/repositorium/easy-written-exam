const express = require('express');
const router = express.Router();
const {authJwt} = require('../middleware');
const multer = require('multer');
const fs = require('fs');
const {
    answerTest,
    getPaperSheetById,
    createPaperSheet
} = require("../controllers/answer.controller");

router.use(function (req, res, next) {
    res.header(
        'Access-Control-Allow-Headers',
        'x-access-token, Origin, Content-Type, Accept'
    );
    next();
});

router.post('/test-submission', [authJwt.verifyToken], answerTest)
router.get('/papersheet/testid/:testId', [authJwt.verifyToken], getPaperSheetById);
router.post('/papersheet/new', [authJwt.verifyToken], createPaperSheet);

module.exports = router;