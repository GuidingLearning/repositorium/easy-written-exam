const Answer = require('../models/answer.model');
const EssayAnswer = require('../models/essay-answer.model');
const Evaluation = require('../models/evaluation.model');
const Feedback = require('../models/feedback.model');
const Inscription = require('../models/inscriptions.model');
const McAnswer = require('../models/mc-answer.model');
const McQuestion = require('../models/mc-question.model');
const PaperSheet = require('../models/papersheet.model');
const TofAnswer = require('../models/tof-answer.model');

const sequelize = require('../models');

const getFeedback = async (req, res) => {
    const {question} = req.params;
    console.log(question)

    const feedbackList = await Feedback.findAll({
        where: {question}
    });

    if (feedbackList) {
        res.json({
            data: feedbackList
        });
    } else {
        res.status(500).json({
            message: 'No feedback found'
        })
    }
}

const getPaperSheetByTest = async (req, res) => {
    const {testId} = req.params;
    const answersByNumber = {}
    const paperSheetsByStudent = {}

    await PaperSheet.findAll({
        where: {
            test: testId
        }
    }).then(async paperSheets => {
        for await (let ps of paperSheets) {
            const answers = await Answer.findAll({
                where: {
                    papersheet: ps.id
                }
            });
            paperSheetsByStudent[ps.student] = ps;
            for await (let answer of answers) {
                let answerDetails;
                if (answer.question_type === 1) {
                    answerDetails = await EssayAnswer.findOne({
                        where: {
                            answer_id: answer.id
                        }
                    });
                } else if (answer.question_type === 2) {
                    answerDetails = await McAnswer.findOne({
                        where: {
                            answer_id: answer.id
                        }
                    });
                } else if (answer.question_type === 3) {
                    answerDetails = await TofAnswer.findOne({
                        where: {
                            answer_id: answer.id
                        }
                    });
                } else {
                    answerDetails = null;
                }
                answer.details = answerDetails;
                answer.revised = false;
                await Evaluation.findOne({where: {answer: answer.id}}).then((evaluation) => {
                    if (evaluation) {
                        answer.revised = true;
                        answer.evaluation = evaluation;
                    }
                })
                if (answersByNumber[answer.question_number]) {
                    answersByNumber[answer.question_number].push(answer);
                } else {
                    answersByNumber[answer.question_number] = [answer];
                }
            }
            ps.answers = answers;
        }
        // Sorting - It can be optimized, pushing correctly in order
        for (let index in answersByNumber) {
            if (answersByNumber[index].length > 1) {
                if (answersByNumber[index][0].question_type === 1) {
                    answersByNumber[index].sort((a, b) => {
                        if (a.details.answer_text === '' || a.details.answer_text === null) {
                            return 1;
                        }
                        if (b.details.answer_text === '' || b.details.answer_text === null) {
                            return -1;
                        }
                        let lenA = a.details.answer_text.replace(/\s\s+/g, ' ').split(' ').length;
                        let lenB = b.details.answer_text.replace(/\s\s+/g, ' ').split(' ').length;
                        if (lenA > lenB) {
                            return -1;
                        } else {
                            return 1;
                        }
                    })
                }

                if (answersByNumber[index][0].question_type === 3) {
                    answersByNumber[index].sort((a, b) => {
                        if (a.details.justification === '' || a.details.justification === null) {
                            return 1;
                        }
                        if (b.details.justification === '' || b.details.justification === null) {
                            return -1;
                        }
                        let lenA = a.details.justification.replace(/\s\s+/g, ' ').split(' ').length;
                        let lenB = b.details.justification.replace(/\s\s+/g, ' ').split(' ').length;
                        if (lenA > lenB) {
                            return -1;
                        } else {
                            return 1;
                        }
                    })
                }
            }
        }

        res.json({
            data: {paperSheets: paperSheetsByStudent, answersByNumber}
        })
    }).catch(e => {
        console.log(e);
        res.status(500).json({
            message: "Something went wrong"
        })
    })
}

const createEvaluation = async (req, res) => {
    const userId = req.userId;
    const {
        answerId,
        grade,
        feedback,
        question,
        tags,
        course,
        isNewFeedback
    } = req.body;

    const finalFeedback = feedback === '' ? null : feedback;

    const t = await sequelize.transaction();

    try {
        const inscription = await Inscription.findByPk(`${course}-${userId}`);
        if (inscription.role !== 2) {
            await t.rollback();
            res.status(401).json({
                message: "Unauthorized"
            })
        } else {
            const existingEvaluation = await Evaluation.findByPk(answerId);
            let evaluation;
            if (existingEvaluation) {
                evaluation = await Evaluation.update({
                    grade,
                    feedback: finalFeedback,
                    evaluator: userId
                }, {
                    where: {
                        id: answerId
                    },
                    transaction: t
                }).catch(async e => {
                    await t.rollback();
                    res.status(500).json({
                        message: 'Something went wrong: ' + e.message
                    })
                })
            } else {
                evaluation = await Evaluation.create({
                    id: answerId,
                    answer: answerId,
                    grade,
                    feedback: finalFeedback,
                    evaluator: userId
                }, {
                    transaction: t
                }).catch(async e => {
                    await t.rollback();
                    res.status(500).json({
                        message: 'Something went wrong: ' + e.message
                    })
                })
            }

            if (feedback && isNewFeedback) {
                let tagList = [];
                if (tags) {
                    tagList = tags;
                }
                await Feedback.create({
                    question,
                    tags: tagList,
                    text: feedback
                }, {
                    fields: [
                        'question',
                        'tags',
                        'text'
                    ],
                    transaction: t
                }).catch(async e => {
                    await t.rollback();
                    res.status(500).json({
                        message: 'Something went wrong: ' + e.message
                    })
                });
            }

            await t.commit();
            res.json({
                data: evaluation
            })
        }
    } catch (e) {
        console.log(e);
        res.status(500).json({
            message: 'Something went wrong: ' + e.message
        })
    }
}

const evaluateMcAnswers = async (req, res) => {
    const userId = req.userId;
    const {
        question,
        answerList,
    } = req.body;

    const t = await sequelize.transaction();

    try {
        const correct_alternative = await McQuestion.findOne({
            where: {
                question
            },
            transaction: t
        }).then(question => question.correct_alternative).catch(async e => {
            await t.rollback();
            res.status(500).json({
                message: 'Question not found: ' + e.message
            })
        });

        if (correct_alternative) {
            for await (let answer of answerList) {
                const grade = correct_alternative === answer.alternative ? 7 : 1;
                await Evaluation.create({
                    id: answer.answer,
                    answer: answer.answer,
                    grade,
                    evaluator: userId
                }, {
                    fields: [
                        'id',
                        'answer',
                        'grade',
                        'evaluator'
                    ],
                    transaction: t
                });
            }
        }

        await t.commit();
        res.json({
            data: 'Answers created'
        })
    } catch (e) {
        await t.rollback();
        res.status(500).json({
            message: 'Evaluation could not be created: ' + e.message
        })
    }
}

module.exports = {
    getFeedback,
    getPaperSheetByTest,
    createEvaluation,
    evaluateMcAnswers
}