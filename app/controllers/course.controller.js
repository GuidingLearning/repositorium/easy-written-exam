const Course = require('../models/course.model');
const Inscription = require('../models/inscriptions.model');
const User = require('../models/user.model');
const {Op} = require('sequelize');
const sequelize = require('../models');

const addNewMember = async (req, res) => {
    const userId = req.userId;
    const {
        course,
        email,
        role
    } = req.body;

    const inscription = await Inscription.findByPk(`${course}-${userId}`);

    if (inscription && inscription.role === 1) {
        const t = await sequelize.transaction();

        try {
            const user = await User.findOne({where: {email}, transaction: t});

            const newInscription = await Inscription.create({
                id: `${course}-${user.id}`,
                course,
                user: user.id,
                role
            }, {
                transaction: t
            });

            const courseToUpdate = await Course.findByPk(course, {transaction: t});

            if (parseInt(role) === 1) {
                let professorList = courseToUpdate.professors ? courseToUpdate.professors : [];
                professorList.push(user.id);
                await Course.update({
                    professors: professorList
                }, {
                    where: {
                        id: course
                    },
                    transaction: t
                });
            } else if (parseInt(role) === 2) {
                let evaluatorList = courseToUpdate.evaluators ? courseToUpdate.evaluators : [];
                evaluatorList.push(user.id);
                await Course.update({
                    evaluators: evaluatorList
                }, {
                    where: {
                        id: course
                    },
                    transaction: t
                });
            } else {
                let studentList = courseToUpdate.students ? courseToUpdate.students : [];
                studentList.push(user.id);
                await Course.update({
                    students: studentList
                }, {
                    where: {
                        id: course
                    },
                    transaction: t
                });
            }


            const userToUpdate = await User.findByPk(user.id, {transaction: t});

            let userInscriptions = userToUpdate.inscriptions ? userToUpdate.inscriptions : [];
            userInscriptions.push(newInscription.id);
            await User.update({
                inscriptions: userInscriptions
            }, {
                where: {
                    id: user.id
                },
                transaction: t
            });

            await t.commit();

            res.json({
                message: 'User added to course'
            })

        } catch (e) {
            await t.rollback();
            console.log(e);
            res.status(500).json({
                message: 'Something went wrong: ' + e.message
            });
        }
    } else {
        res.status(401).json({
            message: 'Unauthorized!'
        })
    }
}

const getCoursesByUserId = async (id, role, res) => {
    const cond = role ? {user: id, role} : {user: id};
    await Inscription.findAll({
        where: cond
    }).then(async inscriptionList => {
        let courses = inscriptionList.map(inscription => inscription.course);
        await Course.findAll({
            where: {
                id: {
                    [Op.in]: courses
                }
            }
        }).then(courseList => {
            res.json({
                data: courseList
            });
        });
    }).catch(e => {
        console.log(e);
        res.status(500).json({
            message: 'Something went wrong.'
        });
    });
};

const getCourses = async (req, res) => {
    await Course.findAll()
        .then(courses => {
            res.json({
                data: courses
            });
        }).catch(e => {
            console.log(e);
            res.status(500).json({
                message: 'Something went wrong.'
            });
        });
};

const getCourseById = async (req, res) => {
    const {courseId} = req.params;
    const userId = req.userId;

    await Course.findByPk(courseId)
        .then(async course => {
            course.professorList = [];
            course.evaluatorList = [];
            course.studentList = [];

            if (course) {
                for await (let professorId of course.professors) {
                    await User.findByPk(professorId).then(user => {
                        if (userId === user.id) {
                            course.editable = true;
                        }
                        course.professorList.push({
                            firstname: user.firstname,
                            lastname: user.lastname
                        })
                    })
                }

                if (course.evaluators) {
                    for await (let evaluatorId of course.evaluators) {
                        await User.findByPk(evaluatorId).then(user => {
                            course.evaluatorList.push({
                                firstname: user.firstname,
                                lastname: user.lastname
                            })
                        })
                    }
                }

                if (course.students) {
                    for await (let studentId of course.students) {
                        await User.findByPk(studentId).then(user => {
                            course.studentList.push({
                                firstname: user.firstname,
                                lastname: user.lastname
                            })
                        })
                    }
                }

                delete course.professors;
                delete course.evaluators;
                delete course.students;

                res.json({
                    data: course
                })
            } else {
                res.status(500).json({
                    message: 'Something went wrong'
                })
            }
        }).catch(e => {
            console.log(e);
            res.status(500).json({
                message: 'Something went wrong'
            })
        });
}

const getCoursesByUniversity = async (req, res) => {
    const {university} = req.params;
    await Course.findAll({
        where: {
            university: university.replace('-', ' - ').replace(/_/g, ' ').toUpperCase()
        }
    }).then(courses => {
        res.json({
            data: courses
        });
    }).catch(e => {
        console.log(e);
        res.status(500).json({
            message: 'Something went wrong.'
        });
    });
};

const getUserCourses = async (req, res) => {
    const {id} = req.params;
    await getCoursesByUserId(id, null, res);
};

const getMyCourses = async (req, res) => {
    const id = req.userId;
    await getCoursesByUserId(id, null, res);
};

const getMyCoursesAsTeacher = async (req, res) => {
    const id = req.userId;
    await getCoursesByUserId(id, 1, res);
}

const joinCourse = async (req, res) => {
    const user = req.userId;
    const {course} = req.body;

    const t = await sequelize.transaction();

    try {
        const inscription = await Inscription.create({
            id: `${course}-${user}`,
            course,
            user,
            role: 3  // TODO fix Student Hardcoded
        }, {
            transaction: t
        });

        const courseToUpdate = await Course.findByPk(course, {transaction: t});

        let studentList = courseToUpdate.students ? courseToUpdate.students : [];
        studentList.push(user);
        const updatedCourse = await Course.update({
            students: studentList
        }, {
            where: {
                id: course
            },
            transaction: t
        });

        const userToUpdate = await User.findByPk(user, {transaction: t});

        let userInscriptions = userToUpdate.inscriptions ? userToUpdate.inscriptions : [];
        userInscriptions.push(inscription.id);
        await User.update({
            inscriptions: userInscriptions
        }, {
            where: {
                id: user
            },
            transaction: t
        });

        await t.commit();

        res.json({
            data: updatedCourse
        })

    } catch (e) {
        await t.rollback();
        console.log(e);
        res.status(500).json({
            message: 'Something went wrong: ' + e.message
        });
    }
};

const createCourse = async (req, res) => {
    const userId = req.userId;

    const {
        name,
        university,
        semester_name,
        semester_number,
        year
    } = req.body;

    // TODO: Fix time zone to Chile

    let nSemester = `0${semester_number.toString()}`;
    let semester = `${nSemester} - ${semester_name}`;
    let [currDate, currTime] = new Date().toISOString().split('T');
    let universityCode = university.replace(' ', '').split('-')[0];

    currDate = currDate.replace(/-/g, '');
    currTime = currTime.split('.')[0].replace(/:/g, '');

    let initialsCandidates = name.split(' ');
    let initials = '';
    initialsCandidates.forEach(i => {
        if (i.length > 1) {
            initials = initials.concat(i[0].toUpperCase());
        }
    })


    let id = universityCode + initials + nSemester + currDate + currTime;
    let code = initials + nSemester + year;

    const t = await sequelize.transaction();

    await Course.create({
        id,
        name,
        university,
        code,
        semester,
        year,
        professors: [userId]
    }, {
        fields: [
            'id',
            'name',
            'university',
            'code',
            'semester',
            'year',
            'professors'
        ],
        transaction: t
    }).then(async (newCourse) => {
        if (newCourse) {
            const course = newCourse.id;
            const id = `${course}-${userId}`;

            return await Inscription.create({
                id,
                course,
                user: userId,
                role: 1
            }, {
                fields: [
                    'id',
                    'course',
                    'user',
                    'role'
                ],
                transaction: t
            });
        } else {
            throw new Error('Course not created');
        }
    }).then(async (newInscription) => {
        if (newInscription) {
            let currUser = await User.findByPk(userId, {raw: false, transaction: t});
            return {newInscription, currUser};
        } else {
            throw new Error('Inscription not created');
        }
    }).then(async ({newInscription, currUser}) => {
        if (currUser) {
            let currInscriptions = currUser.inscriptions ? [...currUser.inscriptions] : [];
            currInscriptions.push(newInscription.id);
            return currUser.update({inscriptions: currInscriptions}, {transaction: t});
        } else {
            throw new Error('User not found');
        }
    }).then(async (updatedUser) => {
        if (updatedUser) {
            await t.commit();
            res.json({
                message: 'Course was created successfully.',
                data: updatedUser.inscriptions
            });
        } else {
            throw new Error('User could not be updated');
        }
    }).catch((e) => {
        t.rollback();
        console.log(e);
        res.status(500).json({
            message: 'Something went wrong: ' + e.message
        });
    });
};

module.exports = {
    addNewMember,
    getCourses,
    getCourseById,
    getCoursesByUniversity,
    getUserCourses,
    getMyCourses,
    getMyCoursesAsTeacher,
    joinCourse,
    createCourse
}