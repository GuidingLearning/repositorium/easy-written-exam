const Answer = require('../models/answer.model');
const EssayAnswer = require('../models/essay-answer.model');
const Inscription = require('../models/inscriptions.model');
const McAnswer = require('../models/mc-answer.model');
const PaperSheet = require('../models/papersheet.model');
const Test = require('../models/test.model');
const TofAnswer = require('../models/tof-answer.model');
const sequelize = require('../models');

const createEssayAnswer = async (answerId, answerText, transaction) => {
    return await EssayAnswer.create({
        answer_id: answerId,
        answer_text: answerText
    }, {
        fields: [
            'answer_id',
            'answer_text',
        ],
        transaction
    });
}

const createMcAnswer = async (answerId, alternative, transaction) => {
    return await McAnswer.create({
        answer_id: answerId,
        alternative: String.fromCharCode('a'.charCodeAt(0) + parseInt(alternative))
    }, {
        fields: [
            'answer_id',
            'alternative'
        ],
        transaction
    });
}

const createTofAnswer = async (answerId, tof, justification, transaction) => {
    return await TofAnswer.create({
        answer_id: answerId,
        tof,
        justification
    }, {
        fields: [
            'answer_id',
            'tof',
            'justification'
        ],
        transaction
    });
}

const createAnswerByType = async (answerId, answerText, justification, type, transaction) => {
    if (type === 1) {  // Essay Answer
        return await createEssayAnswer(answerId, answerText, transaction);
    }

    if (type === 2) {  // MC Answer
        return await createMcAnswer(answerId, answerText, transaction);
    }

    if (type === 3) {  // Tof Answer
        return await createTofAnswer(answerId, answerText, justification, transaction);
    }
}

const createAnswer = async (id, paperSheet, student, questionNumber, questionType, submissionTime, answer, transaction) => {
    await Answer.create({
        id,
        papersheet: paperSheet,
        student,
        question_number: questionNumber,
        question_type: questionType,
        submission_time: submissionTime
    }, {
        transaction
    }).then(async (createdAnswer) => {
        if (createdAnswer) {
            return createAnswerByType(id, answer.answer, answer.justification, questionType, transaction);
        }
    });
}

const answerTest = async (req, res) => {
    const userId = req.userId;
    const {
        questions,
        course,
        testId
    } = req.body;

    let ableToAnswer = false;
    const timeNow = new Date();
    const paperSheetId = parseInt(userId.toString() + testId.toString())

    await Inscription.findOne({
        where: {
            user: userId,
            course
        }
    }).then((inscription) => {
        if (inscription && inscription.role === 3) {
            ableToAnswer = true;
        }
    });

    const paperSheet = await Test.findByPk(testId).then(async (test) => {
        if (test) {
            return await PaperSheet.findByPk(paperSheetId, {raw: false}).then(paperSheet => {
                if (paperSheet) {
                    const diff = Math.round((timeNow - new Date(paperSheet.start_date)) / 1000);
                    if (test.duration - diff < 0) {
                        ableToAnswer = false
                    }
                    return paperSheet;
                }
            });
        }
    })

    if (!ableToAnswer) {
        res.status(401).json({
            message: "Unauthorized"
        });
    }

    const t = await sequelize.transaction();

    try {
        for await (let i of Object.keys(questions)) {
            if (!questions[i].answer && questions[i].answer !== 0) {
                console.log(`Q${i} NOT ANSWERED: ${questions[i].answer}`);
                continue
            }
            await createAnswer(
                parseInt(paperSheetId.toString() + i),
                paperSheetId.toString(),
                userId,
                parseInt(i),
                questions[i].question_type,
                timeNow.toISOString(),
                questions[i],
                t
            );
        }
    } catch (e) {
        console.log(e);
        await t.rollback();
        res.status(500).json({
            message: "Something went wrong"
        });
    }

    await paperSheet.update({
        submission_date: timeNow.toISOString()
    }, {transaction: t}).then(async (updatedPaperSheet) => {
        if (updatedPaperSheet) {
            await t.commit();
            res.json({
                message: 'Test was answered successfully',
                data: updatedPaperSheet
            });
        } else {
            throw new Error('Test could not be answered')
        }
    }).catch(e => {
        t.rollback();
        console.log(e);
        res.status(500).json({
            message: "Something went wrong"
        });
    })

}

const getPaperSheetById = async (req, res) => {
    const student = req.userId;
    const {testId} = req.params;

    await PaperSheet.findByPk(parseInt(student.toString() + testId.toString()))
        .then(paperSheet => {
            res.json({
                data: paperSheet
            })
        }).catch(e => {
            console.log(e);
            res.status(500).json({
                message: 'PaperSheet not found'
            })
        })
}

const createPaperSheet = async (req, res) => {
    const student = req.userId;
    const {testId} = req.body;
    const startDate = new Date();

    await PaperSheet.create({
        id: parseInt(student.toString() + testId.toString()),
        test: testId,
        student,
        start_date: startDate
    }).then(paperSheet => {
        res.json({
            message: 'PaperSheet was created successfully',
            data: paperSheet
        })
    }).catch(e => {
        console.log(e);
        res.status(500).json({
            message: 'Something went wrong',
            message_t_code: 'not-able-to-answer'
        })
    })
}

module.exports = {
    answerTest,
    getPaperSheetById,
    createPaperSheet
}