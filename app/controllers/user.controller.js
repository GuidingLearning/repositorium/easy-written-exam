const User = require('../models/user.model');

const allAccess = (req, res) => {
    res.status(200).send("Public Content.");
};

const userBoard = (req, res) => {
    res.status(200).send("User Content.");
};

const adminBoard = (req, res) => {
    res.status(200).send("Admin Content.");
};

const moderatorBoard = (req, res) => {
    res.status(200).send("Moderator Content.");
};

const getMyInfo = async (req, res) => {
    await User.findByPk(req.userId, {attributes: ['id', 'username', 'firstname', 'lastname', 'birthdate', 'email', 'inscriptions']}).then(user => {
        if (user) {
            res.json({
                data: user
            });
        } else {
            throw new Error('User not found');
        }
    }).catch(e => {
        console.log(e);
        res.status(500).json({
            message: "Something went wrong"
        })
    })
};

const getUsers = async (req, res) => {
    try {
        const users = await User.findAll();

        res.json({
            data: users
        });

    } catch (e) {
        console.log(e);
        res.status(500).json({
            message: 'Something went wrong.'
        });
    }
};

const createUser = async (req, res) => {
    const {
        username,
        firstname,
        lastname,
        birthdate,
        email,
        password,
        ewe_role
    } = req.body;

    // parse birthdate
    let [year, month, day] = birthdate.split('-');
    let birthDate = new Date(parseInt(year), (parseInt(month) - 1), parseInt(day));


    try {
        let newUser = await User.create({
            username,
            firstname,
            lastname,
            birthdate: birthDate,
            email,
            password,
            ewe_role
        }, {
            fields: [
                'username',
                'firstname',
                'lastname',
                'birthdate',
                'email',
                'password',
                'ewe_role'
            ]
        });

        if (newUser) {
            res.json({
                message: 'User was created successfully.',
                data: newUser
            });

        }
    } catch (e) {
        console.log(e);
        res.status(500).json({
            message: 'Something went wrong.'
        });
    }
};

const getUserById = async (req, res) => {
    const {id} = req.params;

    try {
        const user = await User.findOne({
            where: {
                id
            }
        });
        if (user) {
            res.json({
                data: user
            });
        } else {
            res.json({
                message: 'No user with that id was found.'
            });
        }


    } catch (e) {
        console.log(e);
        res.status(500).json({
            message: 'Something went wrong.'
        });
    }
};

const deleteUser = async (req, res) => {
    const {id} = req.params;

    try {
        const deletedRowCount = await User.destroy({
            where: {
                id
            }
        });

        res.json({
            message: 'User was deleted successfully.',
            data: deletedRowCount
        });

    } catch (e) {
        console.log(e);
        res.status(500).json({
            message: 'Something went wrong.'
        });
    }
};

const updateUser = async (req, res) => {
    const {id} = req.params;
    const {
        username,
        firstname,
        lastname,
        birthdate,
        email,
        password,
        ewe_role
    } = req.body;

    try {
        const user = await User.findOne({
            attributes: [
                'id',
                'username',
                'firstname',
                'lastname',
                'birthdate',
                'email',
                'password',
                'ewe_role'
            ],
            where: {
                id
            },
            raw: false
        });

        if (user) {
            await user.update({
                username,
                firstname,
                lastname,
                birthdate,
                email,
                password,
                ewe_role
            });

            res.json({
                message: 'User was updated successfully.',
                data: user
            });
        } else {
            res.json({
                message: 'No role with that id was found.'
            });
        }


    } catch (e) {
        console.log(e);
        res.status(500).json({
            message: 'Something went wrong.'
        });
    }
};

module.exports = {
    allAccess,
    userBoard,
    adminBoard,
    moderatorBoard,
    getMyInfo,
    getUsers,
    createUser,
    getUserById,
    deleteUser,
    updateUser
}