const User = require('../models/user.model');
const RoleEwe = require('../models/role-ewe.model');

const config = require('../config/auth.config');

let jwt = require('jsonwebtoken');
let bcrypt = require('bcryptjs');

const signUp = (req, res) => {
    const ewe_role = req.body.ewe_role ? req.body.ewe_role : 1;

    const {
        firstname,
        lastname,
        email,
        password
    } = req.body;

    let [username, _] = email.split('@');

    // Save User to Database
    User.create({
        username,
        firstname,
        lastname,
        email,
        password: bcrypt.hashSync(password, 8),
        ewe_role
    }, {
        fields: [
            'username',
            'firstname',
            'lastname',
            'email',
            'password',
            'ewe_role'
        ]
    }).then(user => {
        res.json({
            message: "User was registered successfully.",
            message_t_code: "signup.success-message",
            data: user
        });
    }).catch(err => {
        res.status(500).send({message: err.message});
    });
};

const signIn = (req, res) => {
    const {email, password} = req.body;

    User.findOne({
        where: {
            email
        }
    }).then(user => {
        if (!user) {
            return res.status(404).send({
                message: "User Not found.",
                message_t_code: "signin.error-messages.user-not-found"
            });
        }

        let passwordIsValid = bcrypt.compareSync(password, user.password);

        if (!passwordIsValid) {
            return res.status(401).send({
                accessToken: null,
                message: "Invalid Password!",
                message_t_code: "signin.error-messages.invalid-password"
            });
        }

        let token = jwt.sign({id: user.id}, config.secret, {
            expiresIn: 86400 // 24 hours
        });

        let ewe_role_id = user.ewe_role;
        let authority = 'ROLE_';

        RoleEwe.findOne({
            where: {
                id: ewe_role_id
            }
        }).then(ewe_role => {
            authority = authority.concat(ewe_role.name.toUpperCase());

            res.status(200).send({
                id: user.id,
                username: user.username,
                email: user.email,
                roles: authority,
                accessToken: token
            });
        });
    }).catch(err => {
        res.status(500).send({message: err.message});
    });
};

const getRoles = async (req, res) => {
    try {
        const roles = await RoleEwe.findAll();

        res.json({
            data: roles
        });

    } catch (e) {
        console.log(e);
        res.status(500).json({
            message: 'Something went wrong.'
        });
    }
};

const createRole = async (req, res) => {
    const {name, description} = req.body;

    try {
        let newRole = await RoleEwe.create({
            name,
            description
        }, {
            fields: ['name', 'description']
        });

        if (newRole) {
            res.json({
                message: 'Role was created successfully.',
                data: newRole
            });

        }
    } catch (e) {
        console.log(e);
        res.status(500).json({
            message: 'Something went wrong.'
        });
    }
};

const getRoleById = async (req, res) => {
    const {id} = req.params;

    try {
        const role = await RoleEwe.findOne({
            where: {
                id
            }
        });
        if (role) {
            res.json({
                data: role
            });
        } else {
            res.json({
                message: 'No role with that id was found.'
            });
        }


    } catch (e) {
        console.log(e);
        res.status(500).json({
            message: 'Something went wrong.'
        });
    }
};

const deleteRole = async (req, res) => {
    const {id} = req.params;

    try {
        const deletedRowCount = await RoleEwe.destroy({
            where: {
                id
            }
        });

        res.json({
            message: 'Role was deleted successfully.',
            data: deletedRowCount
        });

    } catch (e) {
        console.log(e);
        res.status(500).json({
            message: 'Something went wrong.'
        });
    }
};

const updateRole = async (req, res) => {
    const {id} = req.params;
    const {name, description} = req.body;

    try {
        const role = await RoleEwe.findOne({
            attributes: ['id', 'name', 'description'],
            where: {
                id
            },
            raw: false
        });

        if (role) {
            await role.update({
                name,
                description
            });

            res.json({
                message: 'Role was updated successfully.',
                data: role
            });
        } else {
            res.json({
                message: 'No role with that id was found.'
            });
        }


    } catch (e) {
        console.log(e);
        res.status(500).json({
            message: 'Something went wrong.'
        });
    }
};

module.exports = {
    signIn,
    signUp,
    getRoles,
    createRole,
    getRoleById,
    deleteRole,
    updateRole
}
