const Answer = require('../models/answer.model');
const EssayAnswer = require('../models/essay-answer.model');
const Evaluation = require('../models/evaluation.model');
const McAnswer = require('../models/mc-answer.model');
const TofAnswer = require('../models/tof-answer.model');
const User = require('../models/user.model');

const getMyAnswers = async (req, res) => {
    const student = req.userId;
    const {testId} = req.params;
    const evaluators = {};

    try {
        const answers = await Answer.findAll({
            where: {
                papersheet: student + testId
            }
        })

        if (answers.length > 0) {
            for await (let answer of answers) {
                if (answer.question_type === 1) {
                    answer.details = await EssayAnswer.findOne({
                        where: {
                            answer_id: answer.id
                        }
                    });
                } else if (answer.question_type === 2) {
                    answer.details = await McAnswer.findOne({
                        where: {
                            answer_id: answer.id
                        }
                    });
                } else if (answer.question_type === 3) {
                    answer.details = await TofAnswer.findOne({
                        where: {
                            answer_id: answer.id
                        }
                    });
                } else {
                    answer.details = null;
                }

                await Evaluation.findOne({where: {answer: answer.id}}).then(async (evaluation) => {
                    if (evaluation) {
                        answer.revised = true;
                        answer.evaluation = evaluation;
                        if (!evaluators[evaluation.evaluator]) {
                            evaluators[evaluation.evaluator] = await User.findByPk(evaluation.evaluator);
                        }
                        answer.evaluator = evaluators[evaluation.evaluator];
                    }
                })
            }
        }

        res.json({
            data: answers
        });
    } catch (e) {
        console.log(e);
        res.status(500).json({
            message: 'Something went wrong: ' + e.message
        })
    }
}

module.exports = {
    getMyAnswers
}