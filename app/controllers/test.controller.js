const Course = require('../models/course.model');
const EssayQuestion = require('../models/essay-question.model');
const Inscription = require('../models/inscriptions.model');
const McQuestion = require('../models/mc-question.model');
const PaperSheet = require('../models/papersheet.model');
const Question = require('../models/question.model');
const Test = require('../models/test.model');
const TofQuestion = require('../models/tof-question.model');

const sequelize = require('../models');
const path = require('path');
const fs = require('fs');

const TYPES = {
    "essay": 1,
    "mc": 2,
    "tof": 3
};

const ROLE = {
    1: "professor",
    2: "evaluator",
    3: "student"
}

const ACTION = {
    1: "edit",
    2: "evaluate",
    3: "answer",
    4: "review"
}

const ACTION_LINK = {
    "answer": "/test-answering",
    "edit": "/test-editing",
    "evaluate": "/test-evaluation",
    "review": "/test-review"
}

const getUserTestsFromThisYear = async (req, res) => {
    const userId = req.userId;
    const testList = [];
    const timeNow = new Date();
    const tests = {'0': []};
    const limit = 10;  // Its hardcoded

    let currentTest = 0;
    let currentPage = 0;



    await Inscription.findAll({
        attributes: ['course', 'role'],
        where: {
            user: userId
        }
    }).then(async (inscriptions) => {
        if (inscriptions) {
            for await (let ins of inscriptions) {
                ins.course = await Course.findOne({
                    attributes: ['id', 'name', 'code'],
                    where: {
                        id: ins.course
                    }
                });
                ins.tests = await Test.findAll({
                    attributes: ['id', 'title', 'creation_date', 'start_date', 'deadline_date'],
                    where: {
                        course: ins.course.id
                    }
                });

                for await (let test of ins.tests) {
                    const paperSheet = await PaperSheet.findByPk(parseInt(userId.toString() + test.id.toString()));
                    let status = test.start_date.toISOString() < timeNow.toISOString() && timeNow.toISOString() < test.deadline_date.toISOString() ? "active" : "closed";
                    if (paperSheet && paperSheet.submission_date) {
                        status = "answered";
                    }
                    let action = status === "answered" ? ACTION[4] : ACTION[ins.role];

                    if (ins.role === 1 && test.start_date.toISOString() < timeNow.toISOString()) {
                        action = "";
                    }

                    if (ins.role === 2 && test.start_date.toISOString() > timeNow.toISOString()) {
                        action = "";
                    }

                    if (ins.role === 3 && status === 'closed') {
                        action = "";
                    }

                    const actionLink = ACTION_LINK[action];

                    testList.push({
                        id: test.id,
                        title: test.title,
                        course_id: ins.course.id,
                        course_name: ins.course.name,
                        course_code: ins.course.code,
                        role: ROLE[ins.role],
                        creation_date: test.creation_date,
                        start_date: test.start_date,
                        deadline_date: test.deadline_date,
                        status: status,
                        action: action,
                        action_link: actionLink,
                        answered_at: paperSheet && paperSheet.submission_date ? paperSheet.submission_date : false
                    })
                }
            }

            testList.sort((a, b) => {
                if (a.start_date > b.start_date) {
                    return -1;
                } else {
                    return 1
                }
            })

            for (let tElem of testList) {
                tests[currentPage].push(tElem);
                if (currentTest + 1 === limit) {
                    currentTest = 0;
                    currentPage++;
                    tests[currentPage] = [];
                } else {
                    currentTest++;
                }
            }

            res.json({
                data: tests
            });
        } else {
            throw new Error('No inscriptions found')
        }
    }).catch(e => {
        console.log(e);
        res.status(500).json({
            message: e.message
        });
    })

};

const getTestImg = async (req, res) => {
    const {imgEncodedPath} = req.params;
    try {
        const imgPath = Buffer.from(imgEncodedPath, 'base64').toString('ascii');
        fs.readFile(imgPath, (err, data) => {
            if (data) {
                res.sendFile(imgPath);
            } else {
                console.log(err);
                res.status(401).json({
                    message: 'Error'
                })
            }
        });
    } catch (e) {
        console.log(e);
        res.status(401).json({
            message: 'Error'
        })
    }
}

const getTestById = async (req, res) => {
    const {id} = req.params;
    const userId = req.userId;
    const today = new Date().toISOString();
    let visibleAnswers = true;

    const test = await Test.findByPk(id)
        .catch((e) => {
            console.log(e);
            res.status(500).json({
                message: 'Test not found'
            });
        });

    const role = await Inscription.findByPk(`${test.course}-${userId}`)
        .then(inscription => inscription.role)
        .catch((e) => {
            console.log(e);
            res.status(500).json({
                message: 'Inscription not found'
            });
        });

    if (role === 3) {
        if (today < test.start_date.toISOString()) {
            res.status(401).json({
                message: 'Not allowed!'
            });
        }

        if (today < test.deadline_date.toISOString()) {
            visibleAnswers = false;
        }

    }

    const questions = []

    for await (let qId of test.question_list) {
        await Question.findByPk(qId).then(async (currQuestion) => {
            if (currQuestion.question_type === 1) {
                currQuestion.detailed = await EssayQuestion.findOne({
                    where: {
                        question: currQuestion.id
                    }
                })
            }

            if (currQuestion.question_type === 2) {
                currQuestion.detailed = await McQuestion.findOne({
                    where: {
                        question: currQuestion.id
                    }
                })
            }

            if (currQuestion.question_type === 3) {
                currQuestion.detailed = await TofQuestion.findOne({
                    where: {
                        question: currQuestion.id
                    }
                })
            }

            if (!visibleAnswers) {
                delete (currQuestion.detailed.answer_text);
                delete (currQuestion.detailed.correct_alternative);
                delete (currQuestion.detailed.correct_tof);
                delete (currQuestion.detailed.correct_justification);
            }

            if (currQuestion.detailed.images && currQuestion.detailed.images.length > 0) {
                currQuestion.detailed.imgFiles = [];
                for (let imagePath of currQuestion.detailed.images) {
                    let currPath = path.join(__dirname, imagePath.replace('app', '..'));
                    let hashPath = Buffer.from(currPath).toString('base64');
                    currQuestion.detailed.imgFiles.push(hashPath);
                }
            }
            questions.push(currQuestion);
        }).catch(e => {
            console.log(e);
            res.status(500).json({
                message: "Something went wrong"
            })
        });
    }

    test.question_list = questions;

    res.json({
        data: test
    })
};

const createEssayQuestion = async (question, id, transaction) => {
    const images = [];
    Object.entries(question).map(([key, value]) => {
        if (key.startsWith('image')) {
            images.push(value);
        }
    })
    const imagePaths = images.map(image => image.path);

    return await EssayQuestion.create({
        question: id,
        formulation: question.formulation,
        images: imagePaths,
        answer_text: question.essay_correct_answer
    }, {
        fields: [
            'question',
            'formulation',
            'images',
            'answer_text'
        ],
        transaction
    });
}

const createMcQuestion = async (question, id, transaction) => {
    const images = [];
    const alternatives = [];
    Object.entries(question).map(([key, value]) => {
        if (key.startsWith('image')) {
            images.push(value);
        }
        if (key.startsWith('alternative')) {
            alternatives.push(value)
        }
    })
    const imagePaths = images.map(image => image.path);

    return await McQuestion.create({
        question: id,
        formulation: question.formulation,
        images: imagePaths,
        alternatives,
        correct_alternative: question.correct_alternative
    }, {
        fields: [
            'question',
            'formulation',
            'images',
            'alternatives',
            'correct_alternative'
        ],
        transaction
    });
}

const createTofQuestion = async (question, id, transaction) => {
    return await TofQuestion.create({
        question: id,
        formulation: question.formulation,
        correct_tof: question.correct_tof,
        correct_justification: question.justification
    }, {
        fields: [
            'question',
            'formulation',
            'correct_tof',
            'correct_justification'
        ],
        transaction
    });
}

const createQuestionByType = async (question, id, type, transaction) => {
    if (type === 1) {  // Essay Question
        return await createEssayQuestion(question, id, transaction);
    }

    if (type === 2) {  // MC Question
        return await createMcQuestion(question, id, transaction);
    }

    if (type === 3) {  // Tof Question
        return await createTofQuestion(question, id, transaction);
    }
}

const createTest = async (req, res) => {
    // TODO validation creator is actually the professor of the course
    const {
        test_title,
        course,
        start_date,
        start_time,
        end_date,
        end_time,
        instructions,
        hour,
        min,
        questions
    } = req.body;

    const creator = req.userId;
    const questionList = JSON.parse(questions);

    const h = hour ? hour : 0
    const m = min ? min : 0
    const duration = h * 3600 + m * 60;

    const today = new Date;
    const creation_date = today.toISOString();
    const [startYear, startMonth, startDay] = start_date.split('-');
    const [startHour, startMin] = start_time.split(':');
    const startDate = new Date(startYear, parseInt(startMonth) - 1, startDay, startHour, startMin);
    const startDateString = startDate.toISOString();

    const [endYear, endMonth, endDay] = end_date.split('-');
    const [endHour, endMin] = end_time.split(':');
    const endDate = new Date(endYear, parseInt(endMonth) - 1, endDay, endHour, endMin);
    const endDateString = endDate.toISOString();
    const question_ids = [];

    req.files.forEach(file => {
        let [nQuestion, nImage] = file.fieldname.split('_');
        nQuestion = parseInt(nQuestion.replace('q', ''));
        questionList[nQuestion][nImage] = file;
    })

    const t = await sequelize.transaction();

    for await (let question of questionList) {
        let tags;
        if (question.tag_list) {
            tags = question.tag_list.map(tag => tag.tag);
        }

        await Question.create({
            course,
            creator,
            title: question.title,
            creation_date,
            category: question.category,
            question_type: TYPES[question.type],
            tags
        }, {
            fields: [
                'course',
                'creator',
                'title',
                'creation_date',
                'category',
                'question_type',
                'tags'
            ],
            transaction: t
        }).then(async (createdQuestion) => {
            if (createdQuestion) {
                delete question.image_list; // TODO fix this line (image_list should not even exist)
                return await createQuestionByType(question, createdQuestion.id, createdQuestion.question_type, t);
            } else {
                throw new Error('Question could not be created');
            }
        }).then(async (createdQuestion) => {
            if (createdQuestion) {
                question_ids.push(createdQuestion.question);
            } else {
                throw new Error('Question could not be created');
            }
        }).catch(e => {
            console.log(e);
            t.rollback();
            res.status(500).json({
                message: "Something went wrong"
            });
        });
    }

    await Test.create({
        title: test_title,
        creator,
        course,
        instructions,
        creation_date,
        start_date: startDateString,
        deadline_date: endDateString,
        duration,
        question_list: question_ids
    }, {
        fields: [
            'title',
            'creator',
            'course',
            'instructions',
            'creation_date',
            'start_date',
            'deadline_date',
            'duration',
            'question_list'
        ],
        transaction: t
    }).then(async (createdTest) => {
        if (createdTest) {
            await t.commit();
            res.json({
                message: 'Test was created successfully',
                data: createdTest
            });
        } else {
            throw new Error('Test could not be created');
        }
    }).catch(e => {
        t.rollback();
        console.log(e);
        res.status(500).json({
            message: "Something went wrong"
        });
    });

};

module.exports = {
    getUserTestsFromThisYear,
    getTestImg,
    getTestById,
    createTest
}