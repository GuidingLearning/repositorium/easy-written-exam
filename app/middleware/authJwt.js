const jwt = require("jsonwebtoken");
const config = require("../config/auth.config.js");
const RoleEwe = require('../models/role-ewe.model');
const User = require('../models/user.model');

verifyToken = (req, res, next) => {
    let token = req.headers["x-access-token"];

    if (!token) {
        return res.status(403).send({
            message: "No token provided!"
        });
    }

    jwt.verify(token, config.secret, (err, decoded) => {
        if (err) {
            return res.status(401).send({
                message: "Unauthorized!"
            });
        }
        req.userId = decoded.id;
        next();
    });
};

isAdmin = (req, res, next) => {
    console.log('LOG', req);
    User.findByPk(req.userId).then(user => {
        RoleEwe.findByPk(user.ewe_role).then(ewe_role => {
            if (ewe_role.name === 'admin') {
                next();
                return;
            }
            res.status(403).send({
                message: "Require Admin Role!"
            });
        });
    });
};

isModerator = (req, res, next) => {
    User.findByPk(req.userId).then(user => {
        RoleEwe.findByPk(user.ewe_role).then(ewe_role => {
            if (ewe_role.name === 'moderator') {
                next();
                return;
            }
            res.status(403).send({
                message: "Require Moderator Role!"
            });
        });
    });
};

isModeratorOrAdmin = (req, res, next) => {
    User.findByPk(req.userId).then(user => {
        RoleEwe.findByPk(user.ewe_role).then(ewe_role => {
            if (ewe_role.name === 'admin' || ewe_role.name === 'moderator') {
                next();
                return;
            }
            res.status(403).send({
                message: "Require Moderator or Admin Role!"
            });
        });
    });
};

const authJwt = {
    verifyToken,
    isAdmin,
    isModerator,
    isModeratorOrAdmin
};
module.exports = authJwt;
