const User = require('../models/user.model');

const ROLES = ["user", "admin", "moderator"];

checkDuplicateUsernameOrEmail = (req, res, next) => {
    // Email
    User.findOne({
        where: {
            email: req.body.email
        }
    }).then(user => {
        if (user) {
            res.status(400).send({
                message: "Failed! Email is already in use!",
                message_t_code: "signup.error-messages.email-already-in-use"
            });
            return;
        }
        next();
    });
};

checkRolesExisted = (req, res, next) => {
    if (req.body.role) {
        if (!ROLES.includes(req.body.role)) {
            res.status(400).send({
                message: "Failed! Role does not exist = " + req.body.role
            });
            return;
        }
    }

    next();
};

checkPassword = (req, res, next) => {
    if (!req.body.password || !req.body['repeat-password']) {
        if (req.body.password === '' || req.body['repeat-password'] === '') {
            res.status(400).send({
                message: "Failed! Password and its repetition are required. = " + req.body.role
            });
            return;
        }
    }
    console.log('Verifying similitude')

    if (req.body.password !== req.body['repeat-password']) {
        res.status(400).send({
            message: "Failed! Passwords do not coincide. = " + req.body.role,
            message_t_code: "signup.error-messages.passwords-not-identical"
        });
        return;
    }

    next();
};

const verifySignUp = {
    checkDuplicateUsernameOrEmail,
    checkRolesExisted,
    checkPassword
};

module.exports = verifySignUp;
