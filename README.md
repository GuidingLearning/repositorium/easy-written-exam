# Easy Written Exam
<hr/>

## Description

Online test design platform. Easy Written Exam allows you to design exams for your courses, 
student can easily answer and evaluator can grade their responses.
<hr/>

## Installation

Run `npm install` to install dependencies. Then `cd client` and rerun `npm install` 

## Backend Structure
    .
    ├── app
    ├── client
    ├── .gitignore
    ├── app.js
    ├── index.js
    ├── package.json
    └── README.md

Inside the `app` folder there is the following scaffolding

    .
    ├── controllers
    ├── middleware
    ├── models
    └── routes

### Configuring Jwt and Postgresql connection
Change directory to `app` and create a `config` folder with the files `auth.config.js` and `db.config.js` inside it.

`auth.config.js`
    
    module.exports = {
        secret: "<secret-key>"
    };

`db.config.js`

    module.exports = {
        HOST: "<host>",
        USER: "<user>",
        PASSWORD: "<password>",
        DB: "<db_name>",
        dialect: "postgres",
        pool: {
            max: 5,
            min: 0,
            acquire: 30000,
            idle: 10000
        }
    };

## Frontend Structure

    .
    ├── public
    ├── src
    └── package.json

The `public` folder contains all the public static files. On the other hand, `src` folder contains the following folders 
and scripts:

    .
    ├── actions
    ├── assets
    ├── common
    ├── components
    ├── pages
    ├── reducers
    ├── services
    ├── translations
    ├── App.css
    ├── App.js
    ├── index.js
    ├── store.js
    └── README.md