const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

// importing routes
const authRoutes = require('./app/routes/auth.routes');
const userRoutes = require('./app/routes/user.routes');
const courseRoutes = require('./app/routes/course.routes');
const testRoutes = require('./app/routes/test.routes');
const answerRoutes = require('./app/routes/answer.routes');
const evaluationRoutes = require('./app/routes/evaluation.routes');
const reviewRoutes = require('./app/routes/review.routes');
const path = require('path');

// Uncomment the following lines to generate entity-relationship diagram
// const {writeFileSync} = require('fs');
// const sequelize = require('./app/models');
// const sequelizeErd = require('sequelize-erd');
//
// (async function(){
//
//     const svg = await sequelizeErd({ source: sequelize }); // sequelizeErd() returns a Promise
//     writeFileSync('./erd.svg', svg);
//
//     // Writes erd.svg to local path with SVG file from your Sequelize models
// })();

const app = express();

// let corsOptions = {origin: "http://ewe.local"};  //Change to correct origin url and port
// let corsOptions = {origin: "easywrittenexam.repositorium.cl"};  //Change to correct origin url and port
let corsOptions = {origin: "http://localhost:3000"};  //Change to correct origin url and port


app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended: true}));

// routes
app.use('/api/auth', authRoutes);
app.use('/api/user', userRoutes);
app.use('/api/course', courseRoutes);
app.use('/api/test', testRoutes);
app.use('/api/answer', answerRoutes);
app.use('/api/evaluation', evaluationRoutes);
app.use('/api/review', reviewRoutes);

// simple route
app.get('/', (req, res) => {
    res.json({message: "Welcome to EWE."});
});

// All other GET requests not handled before will return our React app
app.get('/*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'client/build', 'index.html'));
});

module.exports = app;