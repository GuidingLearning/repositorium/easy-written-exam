import React, {useCallback, useEffect} from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import {useDispatch} from 'react-redux';

import {createBrowserHistory} from 'history';

import About from './pages/About';
import Course from './pages/Course';
import Courses from './pages/Courses';
import CreateCourse from './pages/CreateCourse';
import JoinCourse from './pages/JoinCourse';
import Home from './pages/Home';
import NotFound404 from './pages/NotFound404';
import Settings from './pages/Settings';
import SignIn from './pages/SignIn';
import SignUp from './pages/SignUp';
import TestDesign from './pages/TestDesign';
import TestAnswering from './pages/TestAnswering';
import TestEvaluation from './pages/TestEvaluation';
import TestReview from './pages/TestReview';

import EventBus from './common/EventBus';

import {logout} from './actions/auth';

import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css';

const history = createBrowserHistory();

export default function App() {
    const dispatch = useDispatch();

    const logOut = useCallback(() => {
        dispatch(logout());
    }, [dispatch]);

    useEffect(() => {
        EventBus.on("logout", () => {
            logOut();
        })

        return () => {
            EventBus.remove("logout");
        }
    }, [logOut]);

    return (
        <div className="App">
            <Router history={history}>
                <Switch>
                    <Route exact path={["/", "/home"]} component={Home}/>
                    <Route exact path="/course/:courseId" component={Course}/>
                    <Route exact path="/courses" component={Courses}/>
                    <Route exact path="/courses/new" component={CreateCourse}/>
                    <Route exact path="/courses/join" component={JoinCourse}/>
                    <Route exact path="/test-design" component={TestDesign}/>
                    <Route exact path="/test-answering/:testId" component={TestAnswering}/>
                    <Route exact path="/test-evaluation/:testId" component={TestEvaluation}/>
                    <Route exact path="/test-review/:testId" component={TestReview}/>
                    <Route exact path="/login" component={SignIn}/>
                    <Route exact path="/signup" component={SignUp}/>
                    <Route exact path="/about" component={About}/>
                    <Route exact path="/settings" component={Settings}/>
                    <Route path="*" component={NotFound404}/>
                </Switch>
            </Router>
        </div>
    );
};