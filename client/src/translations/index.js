import global_en from './en/global.json';
import global_es from './es/global.json';
import global_fr from './fr/global.json';
import home_en from './en/home.json';
import home_es from './es/home.json';
import home_fr from './fr/home.json';
import test_design_en from './en/test-design.json';
import test_design_es from './es/test-design.json';
import test_design_fr from './fr/test-design.json';
import test_answering_en from './en/test-answering.json';
import test_answering_es from './es/test-answering.json';
import test_answering_fr from './fr/test-answering.json';
import test_evaluation_en from './en/test-evaluation.json';
import test_evaluation_es from './es/test-evaluation.json';
import test_evaluation_fr from './fr/test-evaluation.json';
import test_review_en from './en/test-review.json';
import test_review_es from './es/test-review.json';
import test_review_fr from './fr/test-review.json';
import courses_en from './en/courses.json';
import courses_es from './es/courses.json';
import courses_fr from './fr/courses.json';
import i18next from "i18next";

const lang = localStorage.getItem('lang');
const availableLang = {
    "en": "en",
    "es": "es",
    "fr": "fr",
}

let curr_lang = "es";

if (lang && availableLang[lang]) {
    curr_lang = lang
}

i18next.init({
    interpolation: {escapeValue: false},
    lng: curr_lang,
    resources: {
        en: {
            global: global_en,
            home: home_en,
            test_design: test_design_en,
            test_answering: test_answering_en,
            test_evaluation: test_evaluation_en,
            test_review: test_review_en,
            courses: courses_en
        },
        es: {
            global: global_es,
            home: home_es,
            test_design: test_design_es,
            test_answering: test_answering_es,
            test_evaluation: test_evaluation_es,
            test_review: test_review_es,
            courses: courses_es
        },
        fr: {
            global: global_fr,
            home: home_fr,
            test_design: test_design_fr,
            test_answering: test_answering_fr,
            test_evaluation: test_evaluation_fr,
            test_review: test_review_fr,
            courses: courses_fr
        }
    }
});

export default i18next;