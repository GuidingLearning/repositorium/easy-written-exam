import React, {useState} from 'react';
import {useTranslation} from 'react-i18next';
import axios from 'axios';
import authHeader from '../services/auth-header';
import {Link} from 'react-router-dom';
import {API_URL} from '../common/Constants';

const NewCourseForm = (props) => {
    const [t] = useTranslation("courses");

    const currYear = new Date().getFullYear();

    const [courseInfo, setCourseInfo] = useState({});
    const [message, setMessage] = useState('');
    const [successful, setSuccessful] = useState(false);

    const universityList = [{code: "UCH", name: "UNIVERSIDAD DE CHILE"}]

    const handleCourseInfo = callback => {
        setCourseInfo(callback);
    };

    const handleInputChange = (event) => {
        const target = event.target;
        let name = target.name;
        if (name === 'semester_number') {
            if (target.value <= 1) {
                target.value = 1;
            } else {
                target.value = 2;
            }
        }

        if (name === 'year') {
            if (target.value <= currYear) {
                target.value = currYear;
            }
        }

        handleCourseInfo(courseInfo => ({...courseInfo, [name]: target.value}));
    };

    const onSubmit = () => {
        let form = document.getElementById('course-info-form');
        if (!form.checkValidity()) {
            form.reportValidity();
        } else {
            axios.post(API_URL + 'course/new', courseInfo, {headers: authHeader()})
                .then(() => {
                    setSuccessful(true);
                    setMessage('new.on-success.message');
                })
                .catch((e) => {
                    console.log(e);
                });
        }
    };

    const redirectForm = () => {
        window.location.reload();
    };

    return (
        <div>
            <div className="container mt-3">
                {message && (
                    <div id="signup-messages" className="text-center">
                        <div className={successful ? "success-container" : "error-container"}>
                            {t(message)}
                        </div>
                    </div>
                )}
                {successful ?
                    <div>
                        <button type="button" className="btn btn-success"
                                onClick={redirectForm}>{t("new.on-success.create-course")}
                        </button>
                    </div> :
                    <form id="course-info-form" className="needs-validation" noValidate>
                        <div className="row g-3">
                            <div className="col-12">
                                <label htmlFor="name" className="form-label">{t("new.main-form.name")}</label>
                                <input name="name" type="text" className="form-control" id="name"
                                       value={courseInfo['name'] || ''}
                                       placeholder={t("new.main-form.name-placeholder")}
                                       onChange={handleInputChange} required/>
                            </div>

                            <div className="col-12">
                                <label htmlFor="university"
                                       className="form-label">{t("new.main-form.university")}</label>
                                <select name="university" id="university" className="form-select"
                                        value={courseInfo['university'] || ''} onChange={handleInputChange} required>
                                    <option hidden value="">{t("new.main-form.university-placeholder")}</option>
                                    {universityList.map(university => (
                                        <option value={`${university.code} - ${university.name}`} key={university.code}>
                                            {`${university.code} - ${university.name}`}</option>
                                    ))}
                                </select>
                            </div>

                            <div className="col-sm-1">
                                <label htmlFor="semester-number" className="form-label">#</label>
                                <input name="semester_number" type="number" className="form-control"
                                       id="semester-number"
                                       value={courseInfo['semester_number'] || ''}
                                       min="1" max="2"
                                       placeholder={t("new.main-form.semester-number-placeholder")}
                                       onChange={handleInputChange} required/>
                            </div>

                            <div className="col-sm-8">
                                <label htmlFor="semester-name"
                                       className="form-label">{t("new.main-form.semester-name")}</label>
                                <input name="semester_name" type="text" className="form-control" id="semester-name"
                                       value={courseInfo['semester_name'] || ''}
                                       placeholder={t("new.main-form.semester-name-placeholder")}
                                       onChange={handleInputChange} required/>
                            </div>

                            <div className="col-sm-3">
                                <label htmlFor="year"
                                       className="form-label">{t("new.main-form.year")}</label>
                                <input name="year" type="number" className="form-control" id="year"
                                       value={courseInfo['year'] || ''} onChange={handleInputChange} required/>
                            </div>

                            <div className="col-12 text-center">
                                <button type="button" className="btn btn-primary"
                                        onClick={onSubmit}>{t("new.main-form.create-test")}</button>
                            </div>
                            <div className="col-12 text-lg-end">
                                <Link to="/home">
                                    <button type="button"
                                            className="btn btn-danger">{t("new.main-form.cancel")}</button>
                                </Link>
                            </div>
                        </div>
                    </form>
                }
            </div>
        </div>
    );
};

export default NewCourseForm;
