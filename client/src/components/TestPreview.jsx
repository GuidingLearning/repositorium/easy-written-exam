import React, {useState} from 'react';
import {useTranslation} from 'react-i18next';


const TestPreview = (props) => {
    const [t] = useTranslation("test_answering");

    const [instructionsRead, setInstructionsRead] = useState(false);
    const [currPage, setCurrPage] = useState(0);
    const firstPage = 0;

    const beginTest = () => {
        setInstructionsRead(true);
    }

    const prevPage = () => {
        if (currPage > firstPage) {
            setCurrPage(currPage - 1);
        }
    }

    const nextPage = () => {
        if (currPage < props.testInfo.question_list.length - 1) {
            setCurrPage(currPage + 1);
        }
    }

    const submitTest = () => {
        setCurrPage(0);
        document.getElementById('close-submission-modal').click();
    }


    return (
        <div className="container-fluid">
            <div className="row">
                <div className="col-2">
                    {instructionsRead &&
                    <div>
                        <div className="mt-3">
                            <button type="button" className="btn btn-primary" data-bs-toggle="modal"
                                    data-bs-target="#instructions-modal">
                                {t("left-sidebar.show-instructions")}
                            </button>
                        </div>
                        <div className="modal fade" id="instructions-modal" tabIndex="-1"
                             aria-labelledby="instructions-modal-label" aria-hidden="true">
                            <div className="modal-dialog modal-dialog-centered">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title"
                                            id="instructions-modal-label">{t("left-sidebar.instructions")}</h5>
                                    </div>
                                    <div className="modal-body">
                                        {props.testInfo && props.testInfo.instructions}
                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-secondary"
                                                data-bs-dismiss="modal">{t("left-sidebar.close")}
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    }
                </div>
                <div className="col-8">
                    {props.testInfo &&
                    <div>
                        {!instructionsRead ?
                            <div className="text-center mt-3">
                                <div className="modal-content mt-3">
                                    <div className="card">
                                        <div className="card-body">
                                            <h5 className="card-title">{props.testInfo.test_title}</h5>
                                            <p className="card-text">{props.testInfo.instructions}</p>
                                        </div>
                                    </div>

                                    <div className="container mt-3 mb-3">
                                        <button type="button" className="btn btn-primary"
                                                onClick={beginTest}>{t("content.begin-test")}</button>
                                    </div>
                                </div>
                            </div> :
                            <div>
                                <div className="modal-content mt-3">
                                    <div className="card">
                                        <div className="card-body">
                                            <h5 className="card-title">{`${currPage + 1}) ${props.testInfo.question_list[currPage].title}`}</h5>
                                            <p className="card-text">{props.testInfo.question_list[currPage].formulation}</p>
                                        </div>
                                        <div
                                            className="card-img-container align-content-center text-center overflow-auto">
                                            {props.testInfo.question_list[currPage]["image_list"] && props.testInfo.question_list[currPage]["image_list"].length > 0 && props.testInfo.question_list[currPage]["image_list"].map(image => (
                                                    <img
                                                        id={`question-${props.testInfo.question_list[currPage].number}-img-${image.number}`}
                                                        key={`question-${props.testInfo.question_list[currPage].number}-img-${image.number}`}
                                                        className="card-img-bottom img-preview-wrapper"
                                                        style={{height: "150px", width: "270px"}}
                                                        src={URL.createObjectURL(image.file)}
                                                        alt={image.name}/>
                                            ))}
                                        </div>
                                    </div>


                                    <div className="container mt-3 mb-3">
                                        <form id={`testInfoForm-q${currPage}`} className="needs-validation"
                                              noValidate>
                                            {props.testInfo.question_list[currPage].type === "essay" &&
                                            <div className="row g-3">
                                                <div className="col-12">
                                                    <label htmlFor="text-area">{t("content.answer")}</label>
                                                    <div className="form-floating">
                                                            <textarea className="form-control" id="text-area"
                                                                      name={`res-${currPage}`}
                                                                      style={{height: "120px"}}/>
                                                    </div>
                                                </div>
                                            </div>
                                            }

                                            {props.testInfo.question_list[currPage].type === "mc" &&
                                            <div className="row g-3">
                                                <div className="col-12">
                                                    <label htmlFor="text-area">{t("content.answer")}</label>
                                                    <div className="form-floating">
                                                        <div className="form-check">
                                                            <input type="radio" className="form-check-input"
                                                                   name={`radio-q${currPage}`}
                                                                   id={`radio-q${currPage}-a`}/>
                                                            <label htmlFor={`radio-q${currPage}-a`}
                                                                   className="form-check-label">
                                                                {props.testInfo.question_list[currPage]["alternative_a"]}
                                                            </label>
                                                        </div>
                                                        <div className="form-check">
                                                            <input type="radio" className="form-check-input"
                                                                   name={`radio-q${currPage}`}
                                                                   id={`radio-q${currPage}-b`}/>
                                                            <label htmlFor={`radio-q${currPage}-b`}
                                                                   className="form-check-label">
                                                                {props.testInfo.question_list[currPage]["alternative_b"]}
                                                            </label>
                                                        </div>
                                                        <div className="form-check">
                                                            <input type="radio" className="form-check-input"
                                                                   name={`radio-q${currPage}`}
                                                                   id={`radio-q${currPage}-c`}/>
                                                            <label htmlFor={`radio-q${currPage}-c`}
                                                                   className="form-check-label">
                                                                {props.testInfo.question_list[currPage]["alternative_c"]}
                                                            </label>
                                                        </div>
                                                        <div className="form-check">
                                                            <input type="radio" className="form-check-input"
                                                                   name={`radio-q${currPage}`}
                                                                   id={`radio-q${currPage}-d`}/>
                                                            <label htmlFor={`radio-q${currPage}-d`}
                                                                   className="form-check-label">
                                                                {props.testInfo.question_list[currPage]["alternative_d"]}
                                                            </label>
                                                        </div>
                                                        <div className="form-check">
                                                            <input type="radio" className="form-check-input"
                                                                   name={`radio-q${currPage}`}
                                                                   id={`radio-q${currPage}-e`}/>
                                                            <label htmlFor={`radio-q${currPage}-e`}
                                                                   className="form-check-label">
                                                                {props.testInfo.question_list[currPage]["alternative_e"]}
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            }

                                            {props.testInfo.question_list[currPage].type === "tof" &&
                                            <div className="row g-3">
                                                <div className="col-12">
                                                    <label
                                                        htmlFor={`tof-q${currPage}`}>{t("content.answer")}</label>
                                                    <div className="form-floating">
                                                        <select id={`tof-q${currPage}`}
                                                                name={`res-${currPage}`}>
                                                            <option
                                                                hidden
                                                                value="">{t("content.tof-question.select-option")}</option>
                                                            <option
                                                                value="T">{t("content.tof-question.true")}</option>
                                                            <option
                                                                value="F">{t("content.tof-question.false")}</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div className="col-12">
                                                    <label
                                                        htmlFor={`justification-q${currPage}`}>{t("content.tof-question.justification")}</label>
                                                    <div className="form-floating">
                                                            <textarea className="form-control"
                                                                      id={`justification-q${currPage}`}
                                                                      name={`justification-${currPage}`}
                                                                      style={{height: "120px"}}/>
                                                    </div>
                                                </div>
                                            </div>
                                            }

                                            <button type="button"
                                                    className="btn btn-primary mt-3">{t("content.answer-question")}</button>
                                        </form>
                                    </div>
                                </div>

                                <nav className="mt-3" aria-label="Page navigation example">
                                    <ul className="pagination justify-content-center">
                                        <li className={currPage === firstPage ? "page-item disabled" : "page-item"}>
                                            <button className="page-link" tabIndex="-1"
                                                    onClick={prevPage}>{t("pagination.previous")}</button>
                                        </li>
                                        {props.testInfo.question_list.map((question, index) => (
                                            <li className={currPage === index ? "page-item active" : "page-item"}>
                                                <button className="page-link"
                                                        onClick={() => setCurrPage(index)}>{index + 1}</button>
                                            </li>
                                        ))}
                                        <li className={currPage === props.testInfo.question_list.length - 1 ? "page-item disabled" : "page-item"}>
                                            <button className="page-link"
                                                    onClick={nextPage}>{t("pagination.next")}</button>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        }
                    </div>
                    }
                </div>
                <div className="col-2">
                    {instructionsRead &&
                    <div>
                        <div className="card mt-3">
                            <div className="card-body">
                                <h5 className="card-title">{t("right-sidebar.time-left")}</h5>
                            </div>
                        </div>
                        <div className="card mt-3">
                            <div className="card-body">
                                <h5 className="card-title">{t("right-sidebar.answered-questions")}</h5>
                            </div>
                        </div>
                        <div className="mt-3">
                            <button type="button" className="btn btn-primary" data-bs-toggle="modal"
                                    data-bs-target="#test-submission-modal">
                                {t("right-sidebar.submit-test")}
                            </button>
                        </div>
                        <div className="modal fade" id="test-submission-modal" tabIndex="-1"
                             aria-labelledby="test-submission-modal-label" aria-hidden="true">
                            <div className="modal-dialog modal-dialog-centered">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title"
                                            id="test-submission-modal-label">{t("content.submission-modal.warning")}</h5>
                                    </div>
                                    <div className="modal-body">
                                        {t("content.submission-modal.warning-message")}
                                    </div>
                                    <div className="modal-footer">
                                        <button id="close-submission-modal" type="button"
                                                className="btn btn-secondary"
                                                data-bs-dismiss="modal">{t("content.submission-modal.close")}
                                        </button>
                                        <button type="button" className="btn btn-primary" id="submit-test-preview"
                                                onClick={submitTest}>{t("content.submission-modal.submit")}</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    }
                </div>
            </div>
        </div>
    );
};

export default TestPreview;