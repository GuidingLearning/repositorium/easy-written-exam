import React, {useEffect, useState} from 'react';
import {useTranslation} from 'react-i18next';

import axios from 'axios';
import authHeader from '../services/auth-header';
import {Link} from 'react-router-dom';

const {API_URL} = require('../common/Constants');

const TestInfoForm = (props) => {
    const [t] = useTranslation("test_design");

    const [courseList, setCourseList] = useState([]);

    const handleTestInfo = callback => {
        props.handleTestInfo(callback);
    };

    const handleInputChange = (event) => {
        const target = event.target;
        let name = target.name;
        if (name === 'hour') {
            if (target.value <= 0) {
                target.value = 0;
            } else if (target.value >= 6){
                target.value = 6;
            }
        }

        if (name === 'min') {
            if (target.value <= 0) {
                target.value = 0;
            } else if (target.value >= 59){
                target.value = 59;
            }
        }

        handleTestInfo(testInfo => ({...testInfo, [name]: target.value}));
    };

    useEffect(() => {
        axios.get(API_URL + 'course/mine/for-test-design', {headers: authHeader()}).then((response) => {
            setCourseList(response.data.data);
        });
    }, []);

    return (
        <div>
            <form id="test-info-form" className="needs-validation" noValidate>
                <div className="row g-3">
                    <div className="col-12">
                        <label htmlFor="control-title"
                               className="form-label">{t("content.main-form.title")}</label>
                        <input name="test_title" type="text" className="form-control" id="control-title"
                               value={props.testInfo['test_title'] || ''}
                               placeholder={t("content.main-form.title-placeholder")}
                               onChange={handleInputChange} required/>
                    </div>

                    <div className="col-12">
                        <label htmlFor="course"
                               className="form-label">{t("content.main-form.course")}</label>
                        <select name="course" id="course" className="form-select"
                                value={props.testInfo['course'] || ''} onChange={handleInputChange} required>
                            <option hidden value="">{t("content.main-form.course-placeholder")}</option>
                            {courseList.map(course => (
                                <option value={course.id}
                                        key={course.id}>{course.code} - {course.name} ({course.university})</option>
                            ))}
                        </select>
                    </div>

                    <div className="col-3">
                        <label htmlFor="start-date"
                               className="form-label">{t("content.main-form.start-date")}</label>
                        <input name="start_date" type="date" className="form-control" id="start-date"
                               value={props.testInfo['start_date'] || ''} onChange={handleInputChange} required/>
                    </div>

                    <div className="col-3">
                        <label htmlFor="start-time"
                               className="form-label">{t("content.main-form.start-time")}</label>
                        <input name="start_time" type="time" className="form-control" id="start-time"
                               value={props.testInfo['start_time'] || ''} onChange={handleInputChange} required/>
                    </div>

                    <div className="col-3">
                        <label htmlFor="end-date"
                               className="form-label">{t("content.main-form.end-date")}</label>
                        <input name="end_date" type="date" className="form-control" id="end-date"
                               value={props.testInfo['end_date'] || ''} onChange={handleInputChange} required/>
                    </div>

                    <div className="col-3">
                        <label htmlFor="end-time"
                               className="form-label">{t("content.main-form.end-time")}</label>
                        <input name="end_time" type="time" className="form-control" id="end-time"
                               value={props.testInfo['end_time'] || ''} onChange={handleInputChange} required/>
                    </div>

                    <div className="col-12">
                        <label htmlFor="instructions">{t("content.main-form.instructions")}</label>
                        <div className="form-floating">
                                        <textarea name="instructions" className="form-control"
                                                  placeholder="Escriba las instrucciones para este control"
                                                  id="instructions" style={{height: "120px"}}
                                                  value={props.testInfo['instructions'] || ''}
                                                  onChange={handleInputChange}/>
                        </div>
                    </div>

                    <div className="col-4">
                        <span id="test-duration">{t("content.main-form.test-duration")}</span><br/>
                        <div className="row">
                            <div className="col-6">
                                <label htmlFor="hour">{t("content.main-form.hour")}</label>
                                <input type="number" name="hour" className="form-control"
                                          id="hour" min="0" max="6"
                                          value={props.testInfo['hour'] || 0}
                                          onChange={handleInputChange} required/>
                            </div>

                            <div className="col-6">
                                <label htmlFor="min">{t("content.main-form.min")}</label>
                                <input type="number" name="min" className="form-control"
                                          id="min" min="0" max="59"
                                          value={props.testInfo['min'] || 0}
                                          onChange={handleInputChange} required/>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    );
};

export default TestInfoForm;