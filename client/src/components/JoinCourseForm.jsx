import React, {useState} from 'react';
import {useTranslation} from 'react-i18next';
import axios from 'axios';
import authHeader from '../services/auth-header';
import {Link} from 'react-router-dom';

const {API_URL} = require('../common/Constants');

const JoinCourseForm = (props) => {
    const [t] = useTranslation("courses");

    const [searchInfo, setSearchInfo] = useState({});
    const [hasResults, setHasResults] = useState(false);
    const [courseList, setCourseList] = useState([]);
    const [filteredCourses, setFilteredCourses] = useState([]);

    const universityList = [{code: "UCH", name: "UNIVERSIDAD DE CHILE"}]

    const handleCourseInfo = callback => {
        setSearchInfo(callback);
    };

    const handleInputChange = (event) => {
        const target = event.target;
        let name = target.name;

        if (name === 'name') {
            let filtered = courseList.filter(course => course.name.toLowerCase().includes(target.value.toLowerCase()))
            setFilteredCourses(filtered);
        }

        handleCourseInfo(searchInfo => ({...searchInfo, [name]: target.value}));
    };

    const joinCourse = (course) => {
        axios.post(API_URL + 'course/join', {course: course.id}, {headers: authHeader()})
            .then(response => {
                if (response.data) {
                    course.joined = true;
                    setFilteredCourses([...filteredCourses])
                }
            });
    }

    const searchCourses = (event) => {
        event.preventDefault();
        let form = document.getElementById('course-info-form');
        if (!form.checkValidity()) {
            form.reportValidity();
        } else {
            let university = searchInfo.university.replace(' - ', '-').replace(/ /g, '_').toLowerCase();
            axios.get(`${API_URL}course/university/${university}`)
                .then((response) => {
                    console.log(response.data.data);
                    if (response.data && response.data.data && response.data.data.length > 0) {
                        setHasResults(true);
                        for (let course of response.data.data) {
                            if ((course.students && course.students.includes(props.userId)) ||
                                (course.evaluators && course.evaluators.includes(props.userId)) ||
                                (course.professors && course.professors.includes(props.userId))) {
                                course.joined = true;
                            }
                        }
                        setCourseList(response.data.data);
                        setFilteredCourses(response.data.data);
                    }
                })
                .catch((e) => {
                    console.log(e);
                });
        }
    };

    return (
        <div>
            <div className="container mt-3">
                <div>
                    <form id="course-info-form" className="needs-validation" noValidate>
                        <div className="row g-3">
                            <div className="col-sm-9">
                                <label htmlFor="university"
                                       className="form-label">{t("join.main-form.university")}</label>
                                <select name="university" id="university" className="form-select"
                                        value={searchInfo['university'] || ''} onChange={handleInputChange}
                                        required>
                                    <option hidden value="">{t("join.main-form.university-placeholder")}</option>
                                    {universityList.map(university => (
                                        <option value={`${university.code} - ${university.name}`}
                                                key={university.code}>
                                            {`${university.code} - ${university.name}`}</option>
                                    ))}
                                </select>
                            </div>

                            <div className="col-sm-3">
                                <label className="form-label invisible">.</label>
                                <button type="button" className="btn btn-primary form-control"
                                        onClick={searchCourses}>{t("join.main-form.search")}</button>
                            </div>
                        </div>
                    </form>
                    {hasResults &&
                        <div className="mt-3">
                            <div className="h3">
                                {t("join.results.results")}
                            </div>

                            <div className="col-12 mb-3">
                                <label htmlFor="name" className="form-label">{t("join.results.filter-name")}</label>
                                <input name="name" type="text" className="form-control" id="name"
                                       value={searchInfo['name'] || ''}
                                       placeholder={t("join.results.filter-name-placeholder")}
                                       autoComplete="off"
                                       onChange={handleInputChange}/>
                            </div>

                            <ol className="list-group">
                                {filteredCourses.map((course, index) => (
                                    <li className="list-group-item d-flex justify-content-between align-items-start"
                                        key={index}>
                                        <div className="ms-2 me-auto">
                                            <div className="fw-bold">{course.name}</div>
                                            {course.code}
                                        </div>
                                        {!course.joined ?
                                            <div>
                                                <span className="badge bg-primary rounded-pill pointer"
                                                      onClick={() => {
                                                          document.getElementById(`join-course-btn-${index}`).click();
                                                      }}>{t("join.results.join-button")}</span>

                                                <button id={`join-course-btn-${index}`} type="button"
                                                        className="btn btn-primary visually-hidden"
                                                        data-bs-toggle="modal"
                                                        data-bs-target={`#joinCourseModal-${index}`}>
                                                    Launch demo modal
                                                </button>

                                                <div className="modal fade" id={`joinCourseModal-${index}`}
                                                     tabIndex="-1" aria-labelledby="joinCourseModalLabel"
                                                     aria-hidden="true">
                                                    <div className="modal-dialog">
                                                        <div className="modal-content">
                                                            <div className="modal-header">
                                                                <h5 className="modal-title"
                                                                    id="joinCourseModalLabel">Modal title</h5>
                                                                <button type="button" className="btn-close"
                                                                        data-bs-dismiss="modal" aria-label="Close"/>
                                                            </div>
                                                            <div className="modal-body">
                                                                {t("join.results.modal.message")} <br/>
                                                                {t("join.results.modal.course-name") + ': ' + course.name}
                                                                <br/>
                                                                {t("join.results.modal.course-code") + ': ' + course.code}
                                                            </div>
                                                            <div className="modal-footer">
                                                                <button id={`close-join-modal-btn-${index}`}
                                                                        type="button"
                                                                        className="btn btn-secondary"
                                                                        data-bs-dismiss="modal">{t("join.results.modal.close")}
                                                                </button>
                                                                <button type="button" className="btn btn-primary"
                                                                        onClick={() => {
                                                                            document.getElementById(`close-join-modal-btn-${index}`).click()
                                                                            joinCourse(course)
                                                                        }
                                                                        }>{t("join.results.modal.accept")}
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> :
                                            <span
                                                className="badge bg-success rounded-pill">{t("join.results.joined")}</span>
                                        }
                                    </li>
                                ))}
                            </ol>
                        </div>
                    }

                    <div className="text-end mt-3">
                        <Link to="/home">
                            <button type="button" className="btn btn-danger">{t("join.main-form.back")}</button>
                        </Link>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default JoinCourseForm;
