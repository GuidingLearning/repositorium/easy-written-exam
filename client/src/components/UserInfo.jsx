import React, {useEffect, useState} from 'react';
import {useTranslation} from 'react-i18next';
import axios from 'axios';


import {API_URL} from '../common/Constants';
import authHeader from '../services/auth-header';

const UserInfo = () => {
    const [t] = useTranslation("global");

    const [userInfo, setUserInfo] = useState({});

    useEffect(() => {
        axios.get(API_URL + 'user/my-info', {headers: authHeader()})
            .then(response => {
                if (response.data && response.data.data) {
                    setUserInfo(response.data.data);
                }
            });
    }, []);

    const handleInputChange = () => {};

    return (
        <div className="container-fluid">
            <form id="signup-form"
                  className="needs-validation"
                  method="POST" noValidate>
                <div className="row g-3">
                    <div className="col-sm-6">
                        <label htmlFor="firstname"
                               className="form-label">{t("settings.user-info.firstname")}</label>
                        <input name="firstname"
                               type="text"
                               className="form-control"
                               id="firstname"
                               value={userInfo['firstname'] || ''}
                               placeholder={t("settings.user-info.firstname-placeholder")}
                               onChange={handleInputChange} disabled/>
                    </div>

                    <div className="col-sm-6">
                        <label htmlFor="lastname" className="form-label">{t("settings.user-info.lastname")}</label>
                        <input name="lastname"
                               type="text"
                               className="form-control"
                               id="lastname"
                               value={userInfo['lastname'] || ''}
                               placeholder={t("settings.user-info.lastname-placeholder")}
                               onChange={handleInputChange} disabled/>
                    </div>

                    <div className="col-sm-6">
                        <label htmlFor="username"
                               className="form-label">{t("settings.user-info.username")}</label>
                        <input name="username"
                               type="text"
                               className="form-control"
                               id="username"
                               value={userInfo['username'] || ''}
                               placeholder={t("settings.user-info.username-placeholder")}
                               onChange={handleInputChange} disabled/>
                    </div>

                    {/*<div className="col-3">*/}
                    {/*    <label htmlFor="birthdate"*/}
                    {/*           className="form-label">{t("settings.user-info.birthdate")}</label>*/}
                    {/*    <input name="start_date" type="date" className="form-control" id="birthdate"*/}
                    {/*           value={userInfo['birthdate'] || ''} onChange={handleInputChange} required/>*/}
                    {/*</div>*/}

                    <div className="col-8">
                        <label htmlFor="email" className="form-label">{t("settings.user-info.email")}</label>
                        <input name="email"
                               type="email"
                               className="form-control"
                               id="email"
                               value={userInfo['email'] || ''}
                               onChange={handleInputChange} disabled/>
                    </div>

                    {/*<div className="col-sm-6">*/}
                    {/*    <label htmlFor="password" className="form-label">{t("settings.user-info.password")}</label>*/}
                    {/*    <input name="password"*/}
                    {/*           type="password"*/}
                    {/*           className="form-control"*/}
                    {/*           id="password"*/}
                    {/*           value={userInfo['password'] || ''}*/}
                    {/*           onChange={handleInputChange} required/>*/}
                    {/*</div>*/}

                    {/*<div className="col-sm-6">*/}
                    {/*    <label htmlFor="repeat-password"*/}
                    {/*           className="form-label">{t("settings.user-info.repeat-password")}</label>*/}
                    {/*    <input name="repeat-password"*/}
                    {/*           type="password"*/}
                    {/*           className="form-control"*/}
                    {/*           id="repeat-password"*/}
                    {/*           value={userInfo['repeat-password'] || ''}*/}
                    {/*           onChange={handleInputChange} required/>*/}
                    {/*</div>*/}
                </div>
            </form>
        </div>
    );
};

export default UserInfo;