import React, {useEffect, useState} from 'react';
import {useTranslation} from 'react-i18next';
import {useDispatch} from 'react-redux';
import axios from 'axios';

import ewe_logo from '../assets/ewe_logo.png';

import {logout} from '../actions/auth';
import authHeader from '../services/auth-header';
import EventBus from '../common/EventBus';

const {API_URL} = require('../common/Constants');


const Navbar = () => {
    const [t] = useTranslation("global");
    const dispatch = useDispatch();

    const [userInfo, setUserInfo] = useState();


    useEffect(() => {
        axios.get(API_URL + 'user/my-info', {headers: authHeader()})
            .then(response => {
                if (response.data && response.data.data) {
                    setUserInfo(response.data.data);
                }
            }).catch(e => {
            if (e.response && e.response.status === 401) {
                EventBus.dispatch("logout");
            }
        });
    }, [])

    const logOut = () => {
        dispatch(logout());
    };

    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-light">
            <div className="container">
                <span className="logo">
                    <a className="navbar-brand" href="/">
                        <img src={ewe_logo} alt="ewe_logo" width="71" height="40"/>
                    </a>
                </span>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse"
                        data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false"
                        aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"/>
                </button>
                <div className="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
                    <ul className="navbar-nav">
                        <li className="nav-item dropdown">
                            {userInfo ?
                                <span className="nav-link dropdown-toggle" id="navbarDropdownMenuLink" role="button"
                                      data-bs-toggle="dropdown" aria-expanded="false">
                                    {`${userInfo.firstname} ${userInfo.lastname[0].toUpperCase()}.`}
                                </span> :
                                <span className="nav-link dropdown-toggle" id="navbarDropdownMenuLink" role="button"
                                      data-bs-toggle="dropdown" aria-expanded="false">
                                    {t("navbar.user")}
                                </span>
                            }
                            <ul className="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <li><a className="dropdown-item" href="/courses">{t("navbar.my-courses")}</a></li>
                                <li><a className="dropdown-item" href="/my-tests">{t("navbar.my-tests")}</a></li>
                                <li>
                                    <hr className="dropdown-divider"/>
                                </li>
                                <li><a className="dropdown-item" href="/settings">{t("navbar.settings")}</a></li>
                                <li>
                                    <hr className="dropdown-divider"/>
                                </li>
                                <li><a className="dropdown-item" href="/login" onClick={logOut}>{t("navbar.logout")}</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    );
};

export default Navbar;
