import React, {useEffect, useState} from 'react';
import {useTranslation} from 'react-i18next';
import EssayQuestionForm from './EssayQuestionForm';
import MCQuestionForm from './MCQuestionForm';
import TofQuestionForm from './TofQuestionForm';

const QuestionModalAndButton = (props) => {
    const [t] = useTranslation("test_design");

    const [currentQuestion, setCurrentQuestion] = useState({});

    const [imageList, setImageList] = useState([]);
    const [tagList, setTagList] = useState([]);

    // ------------------------ Start Handling Images ------------------------
    const handleNewImages = callback => {
        setImageList(callback);
    };

    useEffect(() => {
        setCurrentQuestion(currentQuestion => ({...currentQuestion, ["image_list"]: imageList}));
    }, [imageList]);

    const setImages = newImageList => {
        setImageList(newImageList);
    };
    // ------------------------ End Handling Images ------------------------

    // ------------------------ Start Handling Tags ------------------------
    const handleNewTag = callback => {
        setTagList(callback);
    };

    useEffect(() => {
        setCurrentQuestion(currentQuestion => ({...currentQuestion, ["tag_list"]: tagList}));
    }, [tagList]);

    const setTags = newTagList => {
        setTagList(newTagList);
    };
    // ------------------------ End Handling Tags ------------------------

    const addNewQuestion = callback => {
        props.addNewQuestion(callback);
    };

    const createNewQuestion = () => {
        let question = {};
        question.type = props.questionType;
        setCurrentQuestion(question);
    };

    const removeNewQuestion = () => {
        setCurrentQuestion({});
        setImageList([]);
        setTagList([]);
    };

    const handleQuestionFormSubmission = () => {
        let form = document.getElementById(`${props.questionType}-question-form`);
        if (!form.checkValidity()) {
            form.reportValidity();
        } else {
            let closeButton = document.getElementById(`${props.questionType}-form-close`);
            closeButton.click();
            let question = {...currentQuestion}
            question.number = props.questionNumber;
            addNewQuestion(questionList => [...questionList, question]);
            props.incrementQuestionNumber();
            removeNewQuestion();
        }
    };

    let questionForm = '';
    switch (props.questionType) {
        case 'essay':
            questionForm = <EssayQuestionForm
                currentEssayQuestion={currentQuestion}
                handleEssayQuestion={setCurrentQuestion}
                imageList={imageList}
                handleNewImages={handleNewImages}
                setImages={setImages}
                tagList={tagList}
                handleNewTag={handleNewTag}
                setTags={setTags}/>;
            break;
        case 'mc':
            questionForm = <MCQuestionForm
                currentAlternativeQuestion={currentQuestion}
                handleAlternativeQuestion={setCurrentQuestion}
                imageList={imageList}
                handleNewImages={handleNewImages}
                setImages={setImages}
                tagList={tagList}
                handleNewTag={handleNewTag}
                setTags={setTags}/>;
            break;
        case 'tof':
            questionForm = <TofQuestionForm
                currentTofQuestion={currentQuestion}
                handleTofQuestion={setCurrentQuestion}
                imageList={imageList}
                handleNewImages={handleNewImages}
                setImages={setImages}
                tagList={tagList}
                handleNewTag={handleNewTag}
                setTags={setTags}/>;
            break;
        default:
            break;
    }

    return (
        <div>
            <button type="button" className="btn btn-primary" data-bs-toggle="modal"
                    onClick={createNewQuestion}
                    data-bs-target={`#${props.questionType}-question-modal`}>
                {t(`content.${props.questionType}-question`)}
            </button>
            <div className="modal fade" id={`${props.questionType}-question-modal`} data-bs-backdrop="static"
                 data-bs-keyboard="false" tabIndex="-1" aria-labelledby={`${props.questionType}-modal-header`}
                 aria-hidden="true">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title"
                                id={`${props.questionType}-modal-header`}>{t(`content.${props.questionType}-question-title`)}</h5>
                            <button type="button" className="btn-close" data-bs-dismiss="modal"
                                    aria-label="Close"/>
                        </div>
                        <div className="modal-body">
                            {questionForm}
                        </div>
                        <div className="modal-footer">
                            <button id={`${props.questionType}-form-close`} type="button" data-bs-dismiss="modal"
                                    hidden/>
                            <button type="button" className="btn btn-secondary"
                                    data-bs-dismiss="modal"
                                    onClick={removeNewQuestion}>{t("content.question-form.close")}
                            </button>
                            <button type="button" className="btn btn-primary"
                                    onClick={handleQuestionFormSubmission}>{t("content.question-form.accept")}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default QuestionModalAndButton;