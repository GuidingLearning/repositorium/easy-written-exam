import React, {useState} from 'react';
import {useTranslation} from 'react-i18next';

/**
 *  Props:
 *      tagList: List of tags to manage
 *      handleNewTag: Add a new tag to tagList
 *      setTags: set a new value for tagList
 *
 */
const TagsField = (props) => {
    const [t] = useTranslation("test_design");

    const [lastTagNum, setLastTagNum] = useState(0);
    const [currentTag, setCurrentTag] = useState('');

    const handleChange = (event) => {
        setCurrentTag(event.target.value);
    };

    const handleNewTag = callback => {
        props.handleNewTag(callback);
    };

    const setTags = tagList => {
        props.setTags(tagList);
    };

    const handleKeyEnter = (event) => {
        if (event.key === 'Enter') {
            handleTag(event.target.value);
        }
    };

    const handleButtonEnter = (event) => {
        let tagInput = event.target.previousSibling;
        handleTag(tagInput.value);
    };


    const handleTag = (tag) => {
        if (tag === '') {
            return;
        }
        let tagObj = {};
        tagObj.number = lastTagNum;
        tagObj.tag = tag;
        handleNewTag(tags => [...tags, tagObj]);
        setLastTagNum(lastTagNum + 1);
        setCurrentTag('');
    };

    const deleteTag = (e, currentTag) => {
        console.log('TAG', e.target.previousSibling);
        let newTags = props.tagList.filter(tag => tag.number !== currentTag.number);
        setTags(newTags);
    };

    return (
        <div>
            <label htmlFor={`tags`} className="form-label">{t("content.question-form.tags")}</label>
            <div id="tags-container" className="tagListContainer align-content-center d-flex overflow-auto">
                {props.tagList.length > 0 && props.tagList.map(tag => (
                    <div key={tag.number} className="tag-thumbnail-container">
                        <span id={`tag-${tag.number}`} className="tag-preview">{tag.tag}</span>
                        <div className="tag-x-delete"
                             onClick={(event) => deleteTag(event, tag)}>
                            <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" fill="currentColor"
                                 className="bi bi-x-lg" viewBox="0 0 16 16">
                                <path
                                    d="M1.293 1.293a1 1 0 0 1 1.414 0L8 6.586l5.293-5.293a1 1 0 1 1 1.414 1.414L9.414 8l5.293 5.293a1 1 0 0 1-1.414 1.414L8 9.414l-5.293 5.293a1 1 0 0 1-1.414-1.414L6.586 8 1.293 2.707a1 1 0 0 1 0-1.414z"/>
                            </svg>
                        </div>
                    </div>
                ))}
            </div>
            <div className="input-group has-validation">
                <input name={`tags`} id={`tags`} type="text" className="form-control"
                       placeholder={t("content.question-form.tags-placeholder")}
                       value={currentTag} onChange={handleChange} onKeyUp={handleKeyEnter}/>
                <button type="button" className="btn btn-secondary"
                        id="tags-btn" onClick={handleButtonEnter}>
                    {t("content.question-form.add-tag")}
                </button>
            </div>
        </div>
    );
}

export default TagsField;