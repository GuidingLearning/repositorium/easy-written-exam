import React from 'react';
import {useTranslation} from 'react-i18next';
import ImageLoader from './ImageLoader';
import TagsField from './TagsField';

const EssayQuestionForm = (props) => {
    const [t] = useTranslation("test_design");

    const handleEssayQuestion = callback => {
        props.handleEssayQuestion(callback);
    };

    const handleEssayQuestionInputChange = (event) => {
        const target = event.target;
        let name = target.name;
        handleEssayQuestion(currentQuestion => ({...currentQuestion, [name]: target.value}));
    };

    return (
        <div>
            <form id="essay-question-form" className="needs-validation" noValidate>
                <div className="row g-3">
                    {/* question-title */}
                    <div className="col-12">
                        <label htmlFor="essay-title">{t("content.question-form.name")}</label>
                        <input type="text"
                               name="title"
                               value={props.currentEssayQuestion["title"] || ''}
                               className="form-control"
                               placeholder={t("content.question-form.name-placeholder")}
                               id="essay-title"
                               onChange={handleEssayQuestionInputChange}
                               required/>
                    </div>
                    {/* formulation */}
                    <div className="col-12">
                        <label htmlFor="essay-formulation">{t("content.question-form.formulation")}</label>
                        <textarea
                            name="formulation"
                            value={props.currentEssayQuestion["formulation"] || ''}
                            className="form-control"
                            id="essay-formulation" style={{height: "120px"}}
                            onChange={handleEssayQuestionInputChange} required/>
                    </div>
                    {/* images */}
                    <div className="col-12">
                        <ImageLoader
                            imageList={props.imageList}
                            handleNewImages={props.handleNewImages}
                            setImages={props.setImages}/>
                    </div>
                    {/* correct answer */}
                    <div className="col-12">
                        <label htmlFor="essay-correct-answer">{t("content.question-form.correct-answer")}
                            <span className="text-muted"> ({t("content.question-form.optional")})</span></label>
                        <textarea name="essay_correct_answer"
                                  value={props.currentEssayQuestion["essay_correct_answer"] || ''}
                                  className="form-control" id="essay-correct-answer"
                                  style={{height: "120px"}}
                                  onChange={handleEssayQuestionInputChange}/>
                    </div>
                    {/* category */}
                    <div className="col-12">
                        <label htmlFor="essay-category"
                               className="form-label">{t("content.question-form.category")}</label>
                        <input name="category"
                               id="essay-category"
                               value={props.currentEssayQuestion["category"] || ''}
                               type="text" className="form-control"
                               onChange={handleEssayQuestionInputChange}
                               required/>
                    </div>
                    {/* tags */}
                    <div className="col-12">
                        <TagsField tagList={props.tagList} handleNewTag={props.handleNewTag} setTags={props.setTags}/>
                    </div>
                </div>
            </form>
        </div>
    );
};

export default EssayQuestionForm;