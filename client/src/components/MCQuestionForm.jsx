import React from 'react';
import {useTranslation} from 'react-i18next';
import ImageLoader from './ImageLoader';
import TagsField from './TagsField';

const MCQuestionForm = (props) => {
    const [t] = useTranslation("test_design");

    const handleAlternativeQuestion = callback => {
        props.handleAlternativeQuestion(callback);
    };

    const handleAlternativeQuestionInputChange = (event) => {
        const target = event.target;
        let name = target.name;
        handleAlternativeQuestion(currentQuestion => ({...currentQuestion, [name]: target.value}));
    };

    return (
        <div>
            <form id="mc-question-form" className="needs-validation"
                  noValidate>
                <div className="row g-3">
                    {/* question-title */}
                    <div className="col-12">
                        <label htmlFor="alternative-title">{t("content.question-form.name")}</label>
                        <input type="text"
                               name="title"
                               value={props.currentAlternativeQuestion["title"] || ''}
                               className="form-control"
                            // TODO onInvalid={(event) => event.target.setCustomValidity('Completa este campo')}
                               placeholder={t("content.question-form.name-placeholder")}
                               id="alternative-title"
                               onChange={handleAlternativeQuestionInputChange}
                               required/>
                    </div>
                    {/* formulation */}
                    <div className="col-12">
                        <label htmlFor="alternative-formulation">{t("content.question-form.formulation")}</label>
                        <textarea
                            name="formulation"
                            value={props.currentAlternativeQuestion["formulation"] || ''}
                            className="form-control"
                            id="alternative-formulation" style={{height: "120px"}}
                            onChange={handleAlternativeQuestionInputChange}
                            required/>
                    </div>
                    {/* images */}
                    <div className="col-12">
                        <ImageLoader
                            imageList={props.imageList}
                            handleNewImages={props.handleNewImages}
                            setImages={props.setImages}/>
                    </div>
                    {/* alternative A */}
                    <div className="col-12">
                        <label htmlFor="alternative-a"
                               className="form-label">{t("content.question-form.alternative")} A</label>
                        <input name="alternative_a"
                               type="text"
                               id="alternative-a"
                               className="form-control form-control-sm"
                               value={props.currentAlternativeQuestion["alternative_a"] || ''}
                               onChange={handleAlternativeQuestionInputChange}
                               required/>
                    </div>
                    {/* alternative B */}
                    <div className="col-12">
                        <label htmlFor="alternative-b"
                               className="form-label">{t("content.question-form.alternative")} B</label>
                        <input name="alternative_b"
                               type="text"
                               id="alternative-b"
                               className="form-control form-control-sm"
                               value={props.currentAlternativeQuestion["alternative_b"] || ''}
                               onChange={handleAlternativeQuestionInputChange}
                               required/>
                    </div>
                    {/* alternative C */}
                    <div className="col-12">
                        <label htmlFor="alternative-c"
                               className="form-label">{t("content.question-form.alternative")} C</label>
                        <input name="alternative_c"
                               type="text"
                               id="alternative-c"
                               className="form-control form-control-sm"
                               value={props.currentAlternativeQuestion["alternative_c"] || ''}
                               onChange={handleAlternativeQuestionInputChange}
                               required/>
                    </div>
                    {/* alternative D */}
                    <div className="col-12">
                        <label htmlFor="alternative-d"
                               className="form-label">{t("content.question-form.alternative")} D</label>
                        <input name="alternative_d"
                               type="text"
                               id="alternative-d"
                               className="form-control form-control-sm"
                               value={props.currentAlternativeQuestion["alternative_d"] || ''}
                               onChange={handleAlternativeQuestionInputChange}
                               required/>
                    </div>
                    {/* alternative E */}
                    <div className="col-12">
                        <label htmlFor="alternative-e"
                               className="form-label">{t("content.question-form.alternative")} E</label>
                        <input name="alternative_e"
                               type="text"
                               id="alternative-e"
                               className="form-control form-control-sm"
                               value={props.currentAlternativeQuestion["alternative_e"] || ''}
                               onChange={handleAlternativeQuestionInputChange}
                               required/>
                    </div>
                    {/* correct alternative */}
                    <div className="col-12">
                        <label
                            htmlFor="correct-alternative">{t("content.question-form.correct-alternative")}</label>
                        <select name="correct_alternative"
                                id="correct-alternative"
                                className="form-select"
                                value={props.currentAlternativeQuestion["correct_alternative"] || ''}
                                onChange={handleAlternativeQuestionInputChange}
                                required>
                            <option
                                hidden value="">{t("content.question-form.select-option")}</option>
                            <option value="a">A</option>
                            <option value="b">B</option>
                            <option value="c">C</option>
                            <option value="d">D</option>
                            <option value="e">E</option>
                        </select>
                    </div>
                    {/* category */}
                    <div className="col-12">
                        <label
                            htmlFor="alternative-category"
                            className="form-label">{t("content.question-form.category")}</label>
                        <input name="category"
                               id="alternative-category"
                               value={props.currentAlternativeQuestion["category"] || ''}
                               type="text" className="form-control"
                               onChange={handleAlternativeQuestionInputChange}
                               required/>
                    </div>
                    {/* tags */}
                    <div className="col-12">
                        <TagsField tagList={props.tagList} handleNewTag={props.handleNewTag} setTags={props.setTags}/>
                    </div>
                </div>
            </form>
        </div>
    );
};

export default MCQuestionForm;