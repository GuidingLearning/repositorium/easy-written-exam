import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import axios from 'axios';

import {API_URL} from '../common/Constants';
import authHeader from '../services/auth-header';

const CourseList = () => {
    const [courseList, setCourseList] = useState();

    useEffect(() => {
        axios.get(API_URL + 'course/mine/', {headers: authHeader()}).then((response) => {
            setCourseList(response.data.data);
        });
    }, []);

    return (
        <div className="container-fluid mt-3">
            {courseList &&
            <ul className="list-group">
                {courseList.map((course, index) => (
                    <Link to={`course/${course.id}`} key={index} style={{ textDecoration: 'none' }}>
                        <li className="list-group-item hover">
                            {course.name}
                        </li>
                    </Link>
                ))}
            </ul>
            }
        </div>
    );
};

export default CourseList;