import React, {useState} from 'react';
import {useTranslation} from 'react-i18next';

/**
 *  Props:
 *      imageList: List of images to manage
 *      handleNewImages: Add images to imageList
 *      setImages: set a new value for imageList
 *
 */
const ImageLoader = (props) => {
    const [t] = useTranslation("test_design");

    // const [currentImages, setCurrentImages] = useState([]);
    const [lastImgNum, setLastImgNum] = useState(0);

    const handleNewImages = callback => {
        props.handleNewImages(callback);
    };

    const setImages = imagesList => {
        props.setImages(imagesList);
    };

    const handleFiles = (event) => {
        const files = event.target.files;
        let n = lastImgNum;
        for (let i = 0; i < files.length; i++) {
            const file = files[i];

            if (!file.type.startsWith('image/')) {  // If file is not an image
                continue
            }

            let imageObj = {};
            imageObj.number = n++;
            imageObj.file = file;

            handleNewImages(images => [...images, imageObj]);
        }
        event.target.value = null;
        setLastImgNum(n);
    };

    const deleteImage = (e, image) => {
        console.log('IMG', e.target.previousSibling);
        let newImages = props.imageList.filter(img => img.number !== image.number);
        setImages(newImages);
    };

    return (
        <div>
            <label htmlFor="formFileSm" className="form-label">{t("content.question-form.images")}</label>
            <div id="img-container" className="imageListContainer align-content-center d-flex overflow-auto">
                {props.imageList.length > 0 && props.imageList.map(image => (
                    <div key={image.number} className="img-thumbnail-container">
                        <img id={`img-${image.number}`}
                             className="thumbnail-preview"
                             src={URL.createObjectURL(image.file)}
                             alt={image.name}/>
                        <div className="thumbnail-x-delete"
                             onClick={(event) => deleteImage(event, image)}>
                            <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" fill="currentColor"
                                 className="bi bi-x-lg" viewBox="0 0 16 16">
                                <path
                                    d="M1.293 1.293a1 1 0 0 1 1.414 0L8 6.586l5.293-5.293a1 1 0 1 1 1.414 1.414L9.414 8l5.293 5.293a1 1 0 0 1-1.414 1.414L8 9.414l-5.293 5.293a1 1 0 0 1-1.414-1.414L6.586 8 1.293 2.707a1 1 0 0 1 0-1.414z"/>
                            </svg>
                        </div>
                    </div>
                ))}
            </div>
            <input className="form-control form-control-sm" id="formFileSm" type="file" accept="image/*"
                   onChange={handleFiles} multiple/>
        </div>
    );
}

export default ImageLoader;