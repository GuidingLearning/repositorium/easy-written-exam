import React from 'react';
import {useTranslation} from 'react-i18next';
import TagsField from './TagsField';

const TofQuestionForm = (props) => {
    const [t] = useTranslation("test_design");

    const handleTofQuestion = callback => {
        props.handleTofQuestion(callback);
    };

    const handleTofQuestionInputChange = (event) => {
        const target = event.target;
        let name = target.name;
        handleTofQuestion(currentQuestion => ({...currentQuestion, [name]: target.value}));
    };

    return (
        <div>
            <form id="tof-question-form" className="needs-validation" noValidate>
                <div className="row g-3">
                    {/* question-title */}
                    <div className="col-12">
                        <label
                            htmlFor="tof-title">{t("content.question-form.name")}</label>
                        <input type="text"
                               name="title"
                               value={props.currentTofQuestion["title"] || ''}
                               className="form-control"
                               placeholder={t("content.question-form.name-placeholder")}
                               id="tof-title"
                               onChange={handleTofQuestionInputChange}
                               required/>
                    </div>
                    {/* formulation */}
                    <div className="col-12">
                        <label htmlFor="tof-formulation">{t("content.question-form.formulation")}</label>
                        <textarea
                            name={"formulation"}
                            value={props.currentTofQuestion["formulation"] || ''}
                            className="form-control"
                            id="tof-formulation" style={{height: "120px"}}
                            onChange={handleTofQuestionInputChange} required/>
                    </div>
                    {/* correct tof */}
                    <div className="col-12">
                        <label htmlFor="correct-tof">{t("content.question-form.correct-answer")}</label>
                        <select name="correct_tof"
                                value={props.currentTofQuestion["correct_tof"] || ''}
                                className="form-select" id="correct-tof"
                                onChange={handleTofQuestionInputChange} required>
                            <option
                                hidden value="">{t("content.question-form.select-option")}</option>
                            <option
                                value="T">{t("content.question-form.true")}</option>
                            <option
                                value="F">{t("content.question-form.false")}</option>
                        </select>
                    </div>
                    {/* correct justification */}
                    <div className="col-12">
                        <label htmlFor="correct-justification" className="form-label">
                            {t("content.question-form.correct-justification")} <span
                            className="text-muted">({t("content.question-form.optional")})</span></label>
                        <input name="justification"
                               value={props.currentTofQuestion["justification"] || ''}
                               className="form-control form-control-sm"
                               id="correct-justification" type="text"
                               onChange={handleTofQuestionInputChange}/>
                    </div>
                    {/* category */}
                    <div className="col-12">
                        <label htmlFor={"tof-category"}
                               className="form-label">{t("content.question-form.category")}</label>
                        <input name={"category"}
                               id={"tof-category"}
                               value={props.currentTofQuestion["category"] || ''}
                               type="text" className="form-control"
                               onChange={handleTofQuestionInputChange}
                               required/>
                    </div>
                    {/* tags */}
                    <div className="col-12">
                        <TagsField tagList={props.tagList} handleNewTag={props.handleNewTag} setTags={props.setTags}/>
                    </div>
                </div>
            </form>
        </div>
    );
};

export default TofQuestionForm;