import React, {useEffect, useState} from 'react';
import axios from 'axios';

import {Link, Redirect} from 'react-router-dom';
import {useTranslation} from 'react-i18next';
import {useSelector} from 'react-redux';

import Navbar from '../components/Navbar';
import CourseList from '../components/CourseList';

import EventBus from '../common/EventBus';
import authHeader from '../services/auth-header';
const {API_URL} = require('../common/Constants');


export default function Home() {
    const [t] = useTranslation("home");

    const [tests, setTests] = useState({'0': []});
    const [currPage, setCurrPage] = useState(0);
    const [lastPage, setLastPage] = useState();

    const firstPage = 0;
    const {user: currentUser} = useSelector((state) => state.auth);

    useEffect(() => {
        axios.get(API_URL + 'test/mine/home', {headers: authHeader()})
            .then(response => {
                if (response.data && response.data.data) {
                    setTests(response.data.data);
                    setLastPage(Object.keys(response.data.data).length - 1);
                }
            }).catch(e => {
            if (e.response && e.response.status === 401) {
                EventBus.dispatch("logout");
            }
        });
    }, [])

    const prevPage = () => {
        if (currPage > firstPage) {
            setCurrPage(currPage - 1);
        }
    }

    const nextPage = () => {
        if (currPage < lastPage) {
            setCurrPage(currPage + 1);
        }
    }

    if (!currentUser) {
        return <Redirect to="/login"/>;
    }

    return (
        <div>
            <Navbar/>
            <div className="d-flex align-items-start mt-3">
                <div className="nav flex-column nav-pills me-3" id="v-pills-tab" role="tablist"
                     aria-orientation="vertical">
                    <button className="nav-link active" id="v-pills-home-tab" data-bs-toggle="pill"
                            data-bs-target="#v-pills-home" type="button" role="tab" aria-controls="v-pills-home"
                            aria-selected="true">{t("sidebar.home")}
                    </button>
                    <button className="nav-link" id="v-pills-courses-tab" data-bs-toggle="pill"
                            data-bs-target="#v-pills-courses" type="button" role="tab" aria-controls="v-pills-courses"
                            aria-selected="false">{t("sidebar.courses")}
                    </button>
                    <button className="nav-link" id="v-pills-tests-tab" data-bs-toggle="pill"
                            data-bs-target="#v-pills-tests" type="button" role="tab" aria-controls="v-pills-tests"
                            aria-selected="false">{t("sidebar.tests")}
                    </button>
                </div>
                <div className="container-fluid">
                    <div className="tab-content" id="v-pills-tabContent">
                        <div className="tab-pane fade show active" id="v-pills-home" role="tabpanel"
                             aria-labelledby="v-pills-home-tab">
                            {tests[currPage].length <= 0 ?
                                <div className="text-center">
                                    <h3>{t("table.no-test")}</h3>
                                </div> :
                                <div>
                                    <table className="table table-striped table-hover">
                                        <thead className="table-dark">
                                        <tr>
                                            <th scope="col">{t("table.header.course-name")}</th>
                                            <th scope="col">{t("table.header.dates")}</th>
                                            <th scope="col">{t("table.header.status")}</th>
                                            <th scope="col">{t("table.header.role")}</th>
                                            <th scope="col">{t("table.header.action")}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {tests[currPage].map((test, index) => (
                                            <tr key={index}>
                                                <th scope="row">{test.course_name} <br/> {test.title}</th>
                                                <td>{t("table.body.test-from")} {new Date(test.start_date).toLocaleString()}<br/>{t("table.body.test-until")} {new Date(test.deadline_date).toLocaleString()}
                                                </td>
                                                <td>{t(`table.body.${test.status}-status`)}</td>
                                                <td>{t(`table.body.${test.role}-role`)}</td>
                                                <td>
                                                    {test.action &&
                                                    <Link
                                                        to={`${test.action_link}/${test.id}`}>{t(`table.body.${test.action}-action`)}</Link>
                                                    }
                                                </td>
                                            </tr>
                                        ))}
                                        </tbody>
                                    </table>

                                    <nav aria-label="Page navigation example">
                                        <ul className="pagination justify-content-end">
                                            <li className={currPage === firstPage ? "page-item disabled" : "page-item"}>
                                                <button className="page-link" tabIndex="-1"
                                                        onClick={prevPage}>{t("pagination.previous")}</button>
                                            </li>

                                            {Object.keys(tests).map((question, index) => (
                                                <li className={currPage === index ? "page-item active" : "page-item"}>
                                                    <button className="page-link"
                                                            onClick={() => setCurrPage(index)}>{index + 1}</button>
                                                </li>
                                            ))}

                                            <li className={currPage === lastPage ? "page-item disabled" : "page-item"}>
                                                <button className="page-link"
                                                        onClick={nextPage}>{t("pagination.next")}</button>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                            }
                        </div>
                        <div className="tab-pane fade" id="v-pills-courses" role="tabpanel"
                             aria-labelledby="v-pills-courses-tab">
                            <div className="container-fluid">
                                <CourseList/>
                                <div className="row align-content-center m-4">
                                    <div className="col-sm-6">
                                        <Link to="/courses/new">
                                            <button type="button"
                                                    className="btn btn-primary">{t("courses.create-course")}</button>
                                        </Link>
                                    </div>

                                    <div className="col-sm-6">
                                        <Link to="/courses/join">
                                            <button type="button"
                                                    className="btn btn-primary">{t("courses.join-course")}</button>
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="tab-pane fade" id="v-pills-tests" role="tabpanel"
                             aria-labelledby="v-pills-tests-tab">
                            <div className="align-content-center m-4">
                                <Link to="/test-design">
                                    <button type="button"
                                            className="btn btn-primary">{t("tests.design-new-test")}</button>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};