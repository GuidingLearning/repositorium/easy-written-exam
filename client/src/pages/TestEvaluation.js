import React, {useEffect, useState} from 'react';
import {Link, Redirect, useParams} from 'react-router-dom';
import {useSelector} from 'react-redux';
import {useTranslation} from 'react-i18next';
import axios from 'axios';

import '../App.css';
import Navbar from '../components/Navbar';
import authHeader from '../services/auth-header';

const {API_URL} = require('../common/Constants');


export default function TestEvaluation() {
    const [t] = useTranslation("test_evaluation");
    const {testId} = useParams();
    const {user: currentUser} = useSelector((state) => state.auth);

    const [paperSheets, setPaperSheets] = useState();
    const [answersByNumber, setAnswersByNumber] = useState();
    const [currentAnswer, setCurrentAnswer] = useState();
    const [currentAnswerNumber, setCurrentAnswerNumber] = useState();
    const [currentQuestionNumber, setCurrentQuestionNumber] = useState();
    const [testInfo, setTestInfo] = useState();

    const [evaluations, setEvaluations] = useState({});
    const [feedbackList, setFeedbackList] = useState([]);
    const [isNewFeedback, setIsNewFeedback] = useState(false);

    useEffect(() => {
        axios.get(`${API_URL}evaluation/papersheet/${testId}`, {headers: authHeader()})
            .then(response => {
                if (response.data && response.data.data) {
                    console.log(response.data.data);
                    setPaperSheets(response.data.data.paperSheets);
                    setAnswersByNumber(response.data.data.answersByNumber);
                    let evaluationsBuffer = {}
                    for (let i in response.data.data.answersByNumber) {
                        evaluationsBuffer[i] = {}
                        for (let j in response.data.data.answersByNumber[i]) {
                            evaluationsBuffer[i][j] = {
                                answerId: response.data.data.answersByNumber[i][j].id
                            }
                            if (response.data.data.answersByNumber[i][j].evaluation) {
                                evaluationsBuffer[i][j].grade = response.data.data.answersByNumber[i][j].evaluation.grade
                                evaluationsBuffer[i][j].feedback = response.data.data.answersByNumber[i][j].evaluation.feedback
                            } else {
                                evaluationsBuffer[i][j].grade = ''
                                evaluationsBuffer[i][j].feedback = ''
                            }
                        }
                    }
                    setEvaluations(evaluationsBuffer);
                }
            })

        axios.get(API_URL + 'test/testid/' + testId, {headers: authHeader()})
            .then(response => {
                if (response.data && response.data.data) {
                    console.log(response.data.data);
                    setTestInfo(response.data.data);
                }
            });
    }, []);

    const handleInputChange = (event) => {
        const target = event.target;
        let name = target.name;
        if (name === 'grade') {
            if (target.value <= 1) {
                target.value = 1;
            } else if (target.value >= 7) {
                target.value = 7;
            }
        }

        if (name === 'feedback') {
            if (target.value) {
                setIsNewFeedback(true);
            } else {
                setIsNewFeedback(false);
            }
        }

        const evaluationBuffer = {...evaluations}
        evaluationBuffer[currentQuestionNumber][currentAnswerNumber][name] = target.value;
        setEvaluations(evaluationBuffer);
    };

    const selectCurrentAnswer = (index) => {
        setCurrentAnswerNumber(index);
        setCurrentAnswer(answersByNumber[currentQuestionNumber][index]);
    }

    const changeQuestionNumber = (index) => {
        setCurrentQuestionNumber(index);
        setCurrentAnswer(undefined);
        setCurrentAnswerNumber(undefined);

        axios.get(API_URL + 'evaluation/feedback/' + testInfo.question_list[index].id, {headers: authHeader()})
            .then(response => {
                if (response.data && response.data.data) {
                    setFeedbackList(response.data.data);
                }
            }).catch(e => {
            console.log(e.response)
        })
    }

    const handleFeedbackSelection = (feedbackText) => {
        setIsNewFeedback(false);
        let evaluationsBuffer = {...evaluations};
        evaluationsBuffer[currentQuestionNumber][currentAnswerNumber].feedback = feedbackText;
        setEvaluations(evaluationsBuffer);
    }

    const evaluateAnswer = () => {
        const form = document.getElementById('testInfoForm');
        if (!form.checkValidity()) {
            form.reportValidity();
        } else {
            answersByNumber[currentQuestionNumber][currentAnswerNumber].revised = true;
            let evaluationData = {
                ...evaluations[currentQuestionNumber][currentAnswerNumber],
                question: testInfo.question_list[currentQuestionNumber].id,
                tags: testInfo.question_list[currentQuestionNumber].tags,
                course: testInfo.course,
                isNewFeedback
            }
            evaluationData.feedback = evaluationData.feedback.replace(/^\s+|\s+$/g, '');
            if (!evaluationData.feedback) {
                evaluationData.isNewFeedback = false
            }
            console.log(evaluationData);
            axios.post(API_URL + 'evaluation/create', evaluationData, {headers: authHeader()})
                .then(response => {
                    if (response.data) {
                        if (answersByNumber[currentQuestionNumber][currentAnswerNumber + 1]) {
                            setCurrentAnswerNumber(currentAnswerNumber + 1);
                        } else {
                            setCurrentAnswer(null);
                        }
                        setFeedbackList(feedbackList => [...feedbackList, {text: evaluationData.feedback}])
                    }
                }).catch(error => {
                console.log(error.response);
            });
        }
    }

    const evaluateAllMcAnswers = () => {
        const answerList = answersByNumber[currentQuestionNumber].map((answer, index) => {
            answersByNumber[currentQuestionNumber][index].revised = true;
            return {answer: answer.id, alternative: answer.details.alternative};
        })

        setCurrentAnswer(null);

        axios.post(API_URL + 'evaluation/create/mc-answers', {
            question: testInfo.question_list[currentQuestionNumber].id,
            answerList
        }, {headers: authHeader()}).then(response => {
            if (response.data) {
                setCurrentAnswerNumber(null);
                if (answersByNumber[currentQuestionNumber + 1]) {
                    setCurrentQuestionNumber(currentQuestionNumber + 1);
                } else {
                    setCurrentQuestionNumber(0);
                }
            }
        }).catch();
    }


    if (!currentUser) {
        return <Redirect to="/login"/>;
    }

    return (
        <div>
            <Navbar/>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-3">
                        <div className="mt-3">
                            {t("left-sidebar.options-menu.options-menu")}
                            <div className="mt-3">
                                <button type="button" className="btn btn-primary dropdown-toggle"
                                        data-bs-toggle="collapse"
                                        data-bs-target="#collapseExample">{t("left-sidebar.options-menu.by-questions")}
                                </button>
                                <div className="collapse mt-3" id="collapseExample">
                                    <div className="card card-body">
                                        <ol className="list-group">
                                            {testInfo && testInfo.question_list.map((question, index) => (
                                                <li className={"list-group-item pointer d-flex justify-content-between align-items-start" + (index === currentQuestionNumber && " active")}
                                                    key={index}
                                                    onClick={() => changeQuestionNumber(index)}>
                                                    <div className="ms-2 me-auto">
                                                        <div className="fw-bold">{question.title}</div>
                                                    </div>
                                                    <span
                                                        className="badge bg-primary rounded-pill">{t(`left-sidebar.question-type.${question.question_type}`)}</span>
                                                </li>
                                            ))}
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-6">
                        <div className="mt-3">
                            {currentAnswer ?
                                <div>
                                    <div className="card">
                                        <div className="card-body">
                                            <h5 className="card-title">{`${currentQuestionNumber + 1}) ${testInfo.question_list[currentQuestionNumber].title}`}</h5>
                                            <p className="card-text">{testInfo.question_list[currentQuestionNumber].detailed.formulation}</p>
                                            <div className="card-img-container align-content-center text-center">
                                                {
                                                    testInfo.question_list[currentQuestionNumber].detailed.imgFiles &&
                                                    testInfo.question_list[currentQuestionNumber].detailed.imgFiles.map((file, index) => (
                                                        <img id={'img-' + currentQuestionNumber + ' - ' + index}
                                                             src={API_URL + 'test/testimg/' + file} key={index}
                                                             style={{height: "150px", width: "270px"}}
                                                             className="card-img-bottom img-preview-wrapper"
                                                             alt={'img-' + currentQuestionNumber + ' - ' + index}/>
                                                    ))
                                                }
                                            </div>
                                            {testInfo.question_list[currentQuestionNumber].detailed.alternatives &&
                                                testInfo.question_list[currentQuestionNumber].detailed.alternatives.map((alternative, index) => (
                                                    <div className="form-check" key={index}>
                                                        <input type="radio" className="form-check-input"
                                                               name={`radio-q${currentQuestionNumber}`}
                                                               id={`radio-q${currentQuestionNumber}-a${index}`}
                                                               readOnly
                                                               checked={index === answersByNumber[currentQuestionNumber][currentAnswerNumber].details.alternative.charCodeAt(0) - 'a'.charCodeAt(0)}/>
                                                        <label htmlFor={`radio-q${currentQuestionNumber}-a${index}`}
                                                               className="form-check-label">
                                                            {alternative}
                                                        </label>
                                                    </div>
                                                ))}
                                        </div>
                                    </div>

                                    {testInfo && testInfo.question_list[currentQuestionNumber].question_type !== 2 &&
                                        <div>
                                            <div className="card mt-3">
                                                <div className="card-body">
                                                    <h5 className="card-title">{t("content.answer")}</h5>
                                                    {answersByNumber[currentQuestionNumber][currentAnswerNumber].details.answer_text &&
                                                        <p className="card-text">{answersByNumber[currentQuestionNumber][currentAnswerNumber].details.answer_text}</p>
                                                    }

                                                    {answersByNumber[currentQuestionNumber][currentAnswerNumber].details.tof &&
                                                        <p className="card-text">{answersByNumber[currentQuestionNumber][currentAnswerNumber].details.tof}</p>
                                                    }

                                                    {answersByNumber[currentQuestionNumber][currentAnswerNumber].details.justification &&
                                                        <p className="card-text">{answersByNumber[currentQuestionNumber][currentAnswerNumber].details.justification}</p>
                                                    }
                                                </div>
                                            </div>

                                            <div className="mt-3 pt-3">
                                                <form id="testInfoForm" className="needs-validation" noValidate>
                                                    <div className="row g-3">
                                                        <div className="col-3">
                                                            <label htmlFor="gradeInput">{t("content.grade")}</label>
                                                            <input type="number" className="form-control"
                                                                   id="gradeInput"
                                                                   name="grade" onChange={handleInputChange}
                                                                   value={evaluations[currentQuestionNumber][currentAnswerNumber].grade}
                                                                   required/>
                                                        </div>

                                                        <div className="col-9">
                                                            <label htmlFor="feedbackInput">{t("content.feedback")} <span
                                                                className="text-muted">({t("content.optional")})</span></label>
                                                            <div className="form-floating">
                                                    <textarea className="form-control" id="feedbackInput"
                                                              name="feedback" onChange={handleInputChange}
                                                              value={evaluations[currentQuestionNumber][currentAnswerNumber].feedback}
                                                              style={{height: "120px"}}/>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    {answersByNumber[currentQuestionNumber][currentAnswerNumber].revised ?
                                                        <div>
                                                            <button type="button" className="btn btn-primary mt-3"
                                                                    data-bs-toggle="modal"
                                                                    data-bs-target="#exampleModal">
                                                                {t("content.evaluate")}
                                                            </button>

                                                            <div className="modal fade" id="exampleModal" tabIndex="-1"
                                                                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                                <div className="modal-dialog">
                                                                    <div className="modal-content">
                                                                        <div className="modal-header">
                                                                            <h5 className="modal-title"
                                                                                id="exampleModalLabel">{t("content.warning-modal.warning")}</h5>
                                                                            <button type="button" className="btn-close"
                                                                                    data-bs-dismiss="modal"
                                                                                    aria-label="Close"/>
                                                                        </div>
                                                                        <div className="modal-body">
                                                                            {t("content.warning-modal.message")}
                                                                        </div>
                                                                        <div className="modal-footer">
                                                                            <button id="evaluation-modal-close"
                                                                                    type="button"
                                                                                    className="btn btn-secondary"
                                                                                    data-bs-dismiss="modal">{t("content.warning-modal.close")}
                                                                            </button>
                                                                            <button type="button"
                                                                                    onClick={() => {
                                                                                        document.getElementById('evaluation-modal-close').click();
                                                                                        evaluateAnswer();
                                                                                    }}
                                                                                    className="btn btn-primary">{t("content.warning-modal.accept")}
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div> :
                                                        <button type="button" className="btn btn-primary mt-3"
                                                                onClick={evaluateAnswer}>
                                                            {t("content.evaluate")}
                                                        </button>
                                                    }
                                                </form>
                                            </div>
                                        </div>
                                    }
                                </div> :
                                <div>{t("content.no-answer-selected")}</div>
                            }
                        </div>
                    </div>
                    <div className="col-3">
                        <div className="mt-3">
                            {currentQuestionNumber !== undefined ?
                                <div>
                                    {t("right-sidebar.other-responses")}
                                    {paperSheets && answersByNumber && answersByNumber[0] ?
                                        <div className="responses-container">
                                            {answersByNumber[currentQuestionNumber].map((answer, index) => (
                                                <div className="card mt-3" key={`q${answer.question_number}-s${index}`}>
                                                    <div
                                                        className={`card-body pointer ${(index === currentAnswerNumber ? 'selected-answer' : '')}`}
                                                        onClick={() => selectCurrentAnswer(index)}>
                                                        <div className="row">
                                                            {answer.details.answer_text &&
                                                                <div className="col-9">
                                                                    <p className="card-text">{answer.details.answer_text}</p>
                                                                </div>
                                                            }

                                                            {answer.details.alternative &&
                                                                <div className="col-9">
                                                                    <p className="card-text">{answer.details.alternative}</p>
                                                                </div>
                                                            }

                                                            {answer.details.tof &&
                                                                <div className="col-9">
                                                                    <p className="card-text">{answer.details.tof}</p>
                                                                </div>
                                                            }

                                                            <div className="col-3">
                                                                {answer.revised ?
                                                                    <i className="bi bi-check-lg revised-symbol"/> :
                                                                    <i className="bi bi-x-lg unrevised-symbol"/>}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            ))}
                                        </div> :
                                        <div>
                                            {t("right-sidebar.no-answers-yet")}
                                        </div>
                                    }
                                </div> :
                                <div>
                                    {t("right-sidebar.no-question-selected")}
                                </div>
                            }
                        </div>
                        <div className="mt-3">
                            {testInfo && testInfo.question_list[currentQuestionNumber] &&
                            testInfo.question_list[currentQuestionNumber].question_type === 2 ?
                                <div>
                                    {answersByNumber[currentQuestionNumber] &&
                                        answersByNumber[currentQuestionNumber].length > 0 && currentAnswer &&
                                        <button type="button" className="btn btn-success"
                                                onClick={evaluateAllMcAnswers}>{t("right-sidebar.evaluate-all")}
                                        </button>
                                    }
                                </div> :
                                <div>
                                    {feedbackList && feedbackList.length > 0 && currentAnswer &&
                                        <div>
                                            {t("right-sidebar.suggested-feedback")}
                                            <div className="feedback-list-container">
                                                {feedbackList.map((feedback, index) => (
                                                    <div className="card mt-3" key={index}>
                                                        <div className="card-body pointer"
                                                             onClick={() => handleFeedbackSelection(feedback.text)}>
                                                            {feedback.text}
                                                        </div>
                                                    </div>
                                                ))}
                                            </div>
                                        </div>
                                    }
                                </div>
                            }
                        </div>
                    </div>
                </div>
                <div className="text-end mt-3">
                    <Link to="/home">
                        <button type="button" className="btn btn-danger">{t("footer.back")}</button>
                    </Link>
                </div>
            </div>
        </div>
    );
}