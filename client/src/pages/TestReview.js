import React, {useEffect, useState} from 'react';
import {useTranslation} from 'react-i18next';
import {useParams} from 'react-router-dom';
import axios from 'axios';

import Navbar from '../components/Navbar';

import {API_URL} from '../common/Constants';
import authHeader from '../services/auth-header';

const TestReview = () => {
    const [t] = useTranslation("test_review");
    const {testId} = useParams();

    const [testInfo, setTestInfo] = useState();
    const [answers, setAnswers] = useState();

    useEffect(() => {
        axios.get(API_URL + 'review/answers/' + testId, {headers: authHeader()})
            .then(response => {
                if (response.data && response.data.data) {
                    setAnswers(response.data.data);
                    console.log(response.data.data);
                }
            })

        axios.get(API_URL + 'test/testid/' + testId, {headers: authHeader()})
            .then(response => {
                if (response.data && response.data.data) {
                    setTestInfo(response.data.data);
                    console.log(response.data.data);
                }
            });
    }, []);


    return (
        <div>
            <Navbar/>
            <div className="container-fluid text-center">
                {testInfo &&
                <div className="container test-info mt-3">
                    <h2>{testInfo.title}</h2>
                    <h5>{t("instructions")}</h5>
                    <p>{testInfo.instructions}</p>
                </div>
                }

                {testInfo && answers && testInfo.question_list.map((question, index) => (
                    <div key={index}>
                        <div className="row mt-3 align-items-center">
                            <div className="col-sm-8">
                                <div className="card">
                                    <div className="card-body text-start">
                                        <h5 className="card-title">{`${index + 1}) ${question.title}`}</h5>
                                        <p className="card-text">{question.detailed.formulation}</p>
                                        <div className="card-img-container align-content-center text-center">
                                            {
                                                question.detailed.imgFiles &&
                                                question.detailed.imgFiles.map((file, imgIndex) => (
                                                    <img id={'img-' + index + ' - ' + imgIndex}
                                                         src={API_URL + 'test/testimg/' + file} key={imgIndex}
                                                         style={{height: "150px", width: "270px"}}
                                                         className="card-img-bottom img-preview-wrapper"
                                                         alt={'img-' + index + ' - ' + imgIndex}/>
                                                ))
                                            }
                                        </div>
                                        <h5 className="card-title mt-3">{t("test.answer")}</h5>

                                        {answers[index].details.answer_text &&
                                        <p className="card-text">{answers[index].details.answer_text}</p>
                                        }

                                        {question.detailed.alternatives &&
                                        question.detailed.alternatives.map((alternative, choiceIndex) => (
                                            <div className="form-check" key={choiceIndex}>
                                                <input type="radio" className="form-check-input"
                                                       name={`radio-q${index}`}
                                                       id={`radio-q${index}-a${choiceIndex}`}
                                                       readOnly
                                                       checked={choiceIndex === answers[index].details.alternative.charCodeAt(0) - 'a'.charCodeAt(0)}/>
                                                <label htmlFor={`radio-q${index}-a${choiceIndex}`}
                                                       className="form-check-label">
                                                    {alternative}
                                                </label>
                                            </div>
                                        ))}

                                        {answers[index].details.tof &&
                                        <p className="card-text">{answers[index].details.tof}</p>
                                        }

                                        {answers[index].details.justification &&
                                        <p className="card-text">{answers[index].details.justification}</p>
                                        }
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-4 text-start">
                                {answers[index].evaluation ?
                                    <div className="card">
                                        <div className="card-body">
                                            <h4 className="text-muted">{t("evaluations.grade")}: </h4>
                                            <h5 className="card-title">{answers[index].evaluation.grade}</h5>
                                            {answers[index].evaluation.feedback &&
                                            <div>
                                                <h6 className="card-subtitle mb-2 text-muted">{t("evaluations.feedback")}</h6>
                                                <p className="card-text">{answers[index].evaluation.feedback}</p>
                                            </div>
                                            }
                                        </div>
                                        <div className="card-footer">
                                            <span className="text-muted">{t("evaluations.evaluated-by")}: </span>
                                            {answers[index].evaluator.firstname} {answers[index].evaluator.lastname}
                                        </div>
                                    </div> :
                                    <div>{t("evaluations.no-evaluation")}</div>
                                }
                            </div>
                        </div>
                        <hr/>
                    </div>
                ))}
            </div>
        </div>
    );
}

export default TestReview;