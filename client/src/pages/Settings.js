import React from 'react';
import Navbar from '../components/Navbar';
import UserInfo from '../components/UserInfo'


import {useTranslation} from 'react-i18next';

const Settings = () => {
    const [t, i18n] = useTranslation("global");

    const changeLanguage = (lang) => {
        i18n.changeLanguage(lang)
            .then(r => localStorage.setItem('lang', lang));

    }

    return (
        <div>
            <Navbar/>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-2">
                        <div className="mt-3">
                            <div className="nav flex-column nav-pills me-3" id="v-pills-tab" role="tablist"
                                 aria-orientation="vertical">
                                <button className="nav-link active" id="v-pills-user-info-tab" data-bs-toggle="pill"
                                        data-bs-target="#v-pills-user-info" type="button" role="tab"
                                        aria-controls="v-pills-user-info"
                                        aria-selected="false">{t("navbar.user-info")}
                                </button>
                                <button className="nav-link" id="v-pills-language-tab" data-bs-toggle="pill"
                                        data-bs-target="#v-pills-language" type="button" role="tab"
                                        aria-controls="v-pills-language"
                                        aria-selected="true">{t("navbar.language")}
                                </button>
                            </div>

                        </div>
                    </div>
                    <div className="col-8">
                        <div className="mt-3">
                            <div className="tab-content" id="v-pills-tabContent">
                                <div className="tab-pane fade show active" id="v-pills-user-info" role="tabpanel"
                                     aria-labelledby="v-pills-user-info-tab">
                                    <UserInfo/>
                                </div>
                                <div className="tab-pane fade" id="v-pills-language" role="tabpanel"
                                     aria-labelledby="v-pills-language-tab">
                                    <div className="list-group">
                                        <button type="button" className="list-group-item list-group-item-action"
                                                onClick={() => changeLanguage("es")}>{t("languages.spanish")}
                                        </button>
                                        <button type="button" className="list-group-item list-group-item-action"
                                                onClick={() => changeLanguage("en")}>{t("languages.english")}
                                        </button>
                                        <button type="button" className="list-group-item list-group-item-action"
                                                onClick={() => changeLanguage("fr")}>{t("languages.french")}
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-2">
                        <div className="mt-3">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Settings;