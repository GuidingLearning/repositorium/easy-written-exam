import React from 'react';
import NewCourseForm from '../components/NewCourseForm';
import Navbar from '../components/Navbar';
import {useTranslation} from 'react-i18next';

const CreateCourse = () => {
    const [t] = useTranslation("courses");

    return (
        <div>
            <Navbar/>
            <div id="header-title" className="py-5 text-center">
                <h2>{t("new.page-title")}</h2>
            </div>
            <NewCourseForm/>
        </div>
    );
};

export default CreateCourse;