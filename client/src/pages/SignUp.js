import React, {useEffect, useState} from 'react';
import {useTranslation} from 'react-i18next';
import ewe_logo from '../assets/ewe_logo.png';

import {register} from '../actions/auth';
import {useDispatch, useSelector} from 'react-redux';
import {Link} from 'react-router-dom';
import {clearMessage} from '../actions/message';

const SignUp = (props) => {
    const [t, i18n] = useTranslation("global");

    const [formData, setFormData] = useState({});
    const [successful, setSuccessful] = useState(false);

    const {message, message_t_code} = useSelector(state => state.message);
    const dispatch = useDispatch();

    useEffect(() => {
        props.history.listen((location) => {
            dispatch(clearMessage()); // clear message when changing location
        });
    }, []);

    const handleData = callback => {
        setFormData(callback);
    };

    const handleInputChange = (event) => {
        const target = event.target;
        let name = target.name;
        handleData(formData => ({...formData, [name]: target.value}));
    };

    const changeLanguage = (lang) => {
        i18n.changeLanguage(lang)
            .then(r => localStorage.setItem('lang', lang));

    }

    const onSubmit = async (event) => {
        event.preventDefault();
        setSuccessful(false);
        let form = document.getElementById("signup-form");
        if (!form.checkValidity()) {
            form.reportValidity();
        } else {
            dispatch(register(formData))
                .then(() => {
                    setSuccessful(true);
                })
                .catch(() => {
                    setSuccessful(false);
                });
        }
    };

    return (
        <div className="container text-center">
            <div className="form-signup">
                <div id="header-title" className="text-center">
                    <img className="mb-4" src={ewe_logo} alt="" width="192" height="108"/>
                    <h2>{t("signup.title")}</h2>
                </div>
                <div className="row">
                    <div className="col-12">
                        {message_t_code && (
                            <div id="signup-messages" className="text-center">
                                <div className={successful ? "success-container" : "error-container"}>
                                    {t(message_t_code)}
                                </div>
                            </div>
                        )}

                        {successful ?
                            <div>
                                <Link to="/login">
                                    <button type="button" className="btn btn-success">Login</button>
                                </Link>
                            </div> :
                            <form id="signup-form"
                                  className="needs-validation"
                                  method="POST" noValidate>
                                <div className="row g-3">
                                    <div className="col-sm-6">
                                        <label htmlFor="firstname"
                                               className="form-label">{t("signup.firstname")}</label>
                                        <input name="firstname"
                                               type="text"
                                               className="form-control"
                                               id="firstname"
                                               value={formData['firstname'] || ''}
                                               placeholder={t("signup.firstname-placeholder")}
                                               onChange={handleInputChange} required/>
                                    </div>

                                    <div className="col-sm-6">
                                        <label htmlFor="lastname" className="form-label">{t("signup.lastname")}</label>
                                        <input name="lastname"
                                               type="text"
                                               className="form-control"
                                               id="lastname"
                                               value={formData['lastname'] || ''}
                                               placeholder={t("signup.lastname-placeholder")}
                                               onChange={handleInputChange} required/>
                                    </div>

                                    <div className="col-8">
                                        <label htmlFor="email" className="form-label">{t("signup.email")}</label>
                                        <input name="email"
                                               type="email"
                                               className="form-control"
                                               id="email"
                                               value={formData['email'] || ''}
                                               onChange={handleInputChange} required/>
                                    </div>

                                    <div className="col-sm-6">
                                        <label htmlFor="password" className="form-label">{t("signup.password")}</label>
                                        <input name="password"
                                               type="password"
                                               className="form-control"
                                               id="password"
                                               value={formData['password'] || ''}
                                               onChange={handleInputChange} required/>
                                    </div>

                                    <div className="col-sm-6">
                                        <label htmlFor="repeat-password"
                                               className="form-label">{t("signup.repeat-password")}</label>
                                        <input name="repeat-password"
                                               type="password"
                                               className="form-control"
                                               id="repeat-password"
                                               value={formData['repeat-password'] || ''}
                                               onChange={handleInputChange} required/>
                                    </div>

                                    <div className="col-12 mb-3">
                                        <button id="create-test-button"
                                                className="btn btn-primary"
                                                onClick={onSubmit}>{t("signup.signup-button")}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        }
                    </div>
                </div>
            </div>
            <div className="container">
                <nav aria-label="breadcrumb">
                    <ol className="breadcrumb">
                        <li className="breadcrumb-item"><span
                            onClick={() => changeLanguage("es")}>{t("languages.spanish")}</span></li>
                        <li className="breadcrumb-item"><span
                            onClick={() => changeLanguage("en")}>{t("languages.english")}</span></li>
                        <li className="breadcrumb-item"><span
                            onClick={() => changeLanguage("fr")}>{t("languages.french")}</span></li>
                    </ol>
                </nav>
            </div>
        </div>
    )
}

export default SignUp;