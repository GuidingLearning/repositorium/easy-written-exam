import React, {useEffect, useState} from 'react';
import {useParams} from 'react-router-dom';
import {useTranslation} from 'react-i18next';
import axios from 'axios';

import Navbar from '../components/Navbar';

import authHeader from '../services/auth-header';
import {API_URL} from '../common/Constants';

const Course = (props) => {
    const {courseId} = useParams();
    const [t] = useTranslation("courses");

    const [courseInfo, setCourseInfo] = useState();
    const [newMemberData, setNewMemberData] = useState({});
    const [addedMembers, setAddedMembers] = useState(0);

    useEffect(() => {
        axios.get(API_URL + 'course/by-id/' + courseId, {headers: authHeader()}).then(response => {
            console.log(response.data.data);
            if (response.data && response.data.data) {
                setCourseInfo(response.data.data);
            }
        }).catch(e => {
            console.log(e);
        })
    }, [addedMembers])

    const handleInputChange = (event) => {
        const target = event.target;
        const name = target.name;
        setNewMemberData(memberData => ({...memberData, [name]: target.value}));
    }

    const addNewMember = () => {
        document.getElementById('add-member-modal-close').click();
        const data = {...newMemberData, course: courseInfo.id}
        console.log('adding:', data);
        axios.post(API_URL + 'course/add-member/', data, {headers: authHeader()})
            .then(response => {
                console.log(response.data.message);
                setNewMemberData({});
                setAddedMembers(addedMembers + 1);
            })
    }

    return (
        <div>
            <Navbar/>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-2"/>
                    <div className="col-8">
                        {courseInfo &&
                        <div className="course-info mt-3">
                            <div className="course-main-info text-center">
                                <h4>{courseInfo.name}</h4>
                                {courseInfo.code} <br/>
                                {courseInfo.semester.split('-')[1]} <br/>
                                {courseInfo.university.split('-')[1]}
                            </div>
                            <div className="members-container-list">
                                <div className="header mb-3 mt-3">
                                    <div className="row">
                                        <div className="col-8">
                                            {t("content.members")}
                                        </div>
                                        {courseInfo.editable &&
                                        <div className="col-4 text-end">
                                            <button type="button" className="btn btn-primary" data-bs-toggle="modal"
                                                    data-bs-target="#addMemberModal">
                                                <i className="bi bi-plus"/>
                                            </button>

                                            <div className="modal fade text-start" id="addMemberModal" tabIndex="-1"
                                                 aria-labelledby="addMemberModalLabel" aria-hidden="true">
                                                <div className="modal-dialog">
                                                    <div className="modal-content">
                                                        <div className="modal-header">
                                                            <h5 className="modal-title" id="addMemberModalLabel">{t("content.modal.new-member")}</h5>
                                                            <button type="button" className="btn-close"
                                                                    data-bs-dismiss="modal" aria-label="Close"/>
                                                        </div>
                                                        <div className="modal-body">
                                                            <form id="new-course-member">
                                                                <div className="row">
                                                                    <div className="col-12">
                                                                        <label htmlFor="email"
                                                                               className="form-label">{t("content.modal.email")}</label>
                                                                        <input name="email" type="text"
                                                                               className="form-control" id="email"
                                                                               value={newMemberData['email'] || ''}
                                                                               placeholder={t("content.modal.email-placeholder")}
                                                                               onChange={handleInputChange} required/>
                                                                    </div>

                                                                    <div className="col-12">
                                                                        <label htmlFor="role"
                                                                               className="form-label">{t("content.modal.role")}</label>
                                                                        <select name="role" id="role"
                                                                                className="form-select"
                                                                                value={newMemberData['role'] || ''}
                                                                                onChange={handleInputChange} required>
                                                                            <option hidden
                                                                                    value="">{t("content.modal.role-placeholder")}</option>
                                                                            <option
                                                                                value={1}>{t("content.modal.professor-option")}</option>
                                                                            <option
                                                                                value={2}>{t("content.modal.evaluator-option")}</option>
                                                                            <option
                                                                                value={3}>{t("content.modal.student-option")}</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                        <div className="modal-footer">
                                                            <button id="add-member-modal-close" type="button"
                                                                    className="btn btn-secondary"
                                                                    data-bs-dismiss="modal">{t("content.modal.close")}
                                                            </button>
                                                            <button type="button" className="btn btn-primary"
                                                                    onClick={addNewMember}>{t("content.modal.add")}
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        }
                                    </div>
                                </div>
                                <div className="professors-list">
                                    <h5>{t("content.professors")}</h5>
                                    <ul className="list-group list-group-flush">
                                        {courseInfo.professorList.map((user, index) => (
                                            <li className="list-group-item bg-transparent"
                                                key={index}>{user.firstname} {user.lastname}</li>
                                        ))}
                                    </ul>
                                </div>
                                <div className="evaluators-list">
                                    <h5>{t("content.evaluators")}</h5>
                                    <ul className="list-group list-group-flush">
                                        {courseInfo.evaluatorList.length > 0 ?
                                            courseInfo.evaluatorList.map((user, index) => (
                                                <li className="list-group-item bg-transparent"
                                                    key={index}>{user.firstname} {user.lastname}</li>
                                            )) :
                                            <li className="list-group-item bg-transparent">No hay</li>}
                                    </ul>
                                </div>
                                <div className="students-list">
                                    <h5>{t("content.students")}</h5>
                                    <ul className="list-group list-group-flush">
                                        {courseInfo.studentList.length > 0 ?
                                            courseInfo.studentList.map((user, index) => (
                                                <li className="list-group-item bg-transparent"
                                                    key={index}>{user.firstname} {user.lastname}</li>
                                            )) :
                                            <li className="list-group-item bg-transparent">No hay</li>}
                                    </ul>
                                </div>
                            </div>
                        </div>
                        }

                    </div>
                    <div className="col-2"/>
                </div>
            </div>
        </div>
    );
}

export default Course;