import React, {useEffect, useState} from 'react';
import {Link, Redirect} from 'react-router-dom';
import {useSelector} from 'react-redux';
import axios from 'axios';

import {useTranslation} from 'react-i18next';

import Navbar from '../components/Navbar';
import TestInfoForm from '../components/TestInfoForm';
import TestPreview from '../components/TestPreview';
import QuestionModalAndButton from '../components/QuestionModalAndButton';

import authHeader from '../services/auth-header';
import '../App.css';

const {API_URL} = require('../common/Constants');


export default function TestDesign() {
    const [t] = useTranslation("test_design");

    const [testInfo, setTestInfo] = useState({});
    const [errorMessage, setErrorMessage] = useState('');
    const [successful, setSuccessful] = useState(false);

    const handleTestInfo = callback => {
        setTestInfo(callback);
    }

    const [questionNumber, setQuestionNumber] = useState(0);

    const [questionList, setQuestionList] = useState([]);


    // ------------------------ Start Handling Questions ------------------------
    const handleNewQuestions = callback => {
        setQuestionList(callback);
    };

    useEffect(() => {
        setTestInfo(testInfo => ({...testInfo, ["question_list"]: questionList}));
    }, [questionList])

    const incrementQuestionNumber = () => {
        setQuestionNumber(questionNumber + 1);
    };

    const deleteQuestion = (event, question) => {
        let currQuestionList = questionList.filter(currQuestion => currQuestion.number !== question.number);
        console.log(currQuestionList);

        for (let i = question.number; i < currQuestionList.length; i++) {
            currQuestionList[i].number = i;
        }
        console.log(currQuestionList);
        setQuestionNumber(questionNumber - 1);
        setQuestionList(currQuestionList);
    }

    // ------------------------ End Handling Questions ------------------------

    // ------------------------ Validations ------------------------

    const validateCourseIsSelected = () => {
        if (testInfo['course']) {
            return testInfo['course'] !== '';
        }
        return false;
    };

    const validateDates = () => {
        let startDate = testInfo['start_date'];
        let startTime = testInfo['start_time'];
        let endDate = testInfo['end_date'];
        let endTime = testInfo['end_time'];

        if (startDate > endDate) {
            return false;
        }

        if (startDate === endDate) {
            if (startTime >= endTime) {
                return false;
            }
        }

        return true;
    };

    const validateDuration = () => {
        const h = testInfo['hour'] ? testInfo['hour'] : 0
        const m = testInfo['min'] ? testInfo['min'] : 0
        const s = testInfo['sec'] ? testInfo['sec'] : 0
        return (h + m + s) > 0
    }

    const testHasQuestions = () => {
        return questionList.length > 0;
    };

    // ------------------------ Validations ------------------------

    function parseTestInfo(testInfo) {
        let formData = new FormData();
        Object.entries(testInfo).forEach(([key, value]) => {
            formData.append(key, value.toString());
        });

        formData.append('questions', JSON.stringify(testInfo.question_list));

        testInfo.question_list.forEach(question => {
            if (question.image_list) {
                question.image_list.forEach(image => {
                    formData.append(`q${question.number}_image${image.number}`, image.file);
                });
            }
        })

        return formData;
    }

    const previewTest = () => {
        console.log(testInfo);
        console.log(questionNumber);
    }

    const askForSubmit = (event) => {
        event.preventDefault();
        let form = document.getElementById("test-info-form");
        if (!form.checkValidity()) {
            form.reportValidity();
        } else {
            let modalButton = document.getElementById('accept-modal-btn');
            modalButton.click();
        }
    }

    const onSubmit = (event) => {
        let closeButton = document.getElementById('close-accept-modal-btn');
        closeButton.click();
        event.preventDefault();
        let form = document.getElementById("test-info-form");
        if (!form.checkValidity()) {
            form.reportValidity();
        } else {
            if (!validateCourseIsSelected()) {
                setErrorMessage('content.alert-messages.must-choose-course');
            } else {
                if (!validateDates()) {
                    setErrorMessage('content.alert-messages.invalid-date');
                } else {
                    if (!validateDuration()) {
                        setErrorMessage('content.alert-messages.invalid-duration')
                    } else {
                        if (!testHasQuestions()) {
                            setErrorMessage('content.alert-messages.no-questions');
                        } else {
                            const data = parseTestInfo(testInfo);
                            axios.post(API_URL + 'test/new', data, {headers: authHeader()})
                                .then((res) => {
                                    setErrorMessage('content.on-success.message');
                                    setSuccessful(true);
                                    console.log(res);
                                })
                                .catch((e) => {
                                    console.log(e);
                                });
                        }
                    }
                }
            }
        }
    };

    const {user: currentUser} = useSelector((state) => state.auth);

    if (!currentUser) {
        return <Redirect to="/login"/>;
    }

    const redirectForm = () => {
        window.location.reload();
    };

    return (
        <div>
            <Navbar/>
            <div id="test-design-content" className="container">
                <div id="header-title" className="py-5 text-center">
                    <h2>{t("header.test-design-title")}</h2>
                    <p className="lead">{t("header.test-design-subtitle")}</p>
                </div>
                {errorMessage && (
                    <div id="signup-messages" className="text-center">
                        <div className={successful ? "success-container" : "error-container"}>
                            {t(errorMessage)}
                        </div>
                    </div>
                )}
                {successful ?
                    <div>
                        <div className="d-flex">
                            <div className="row justify-content-between">
                                <div className="col-sm-6">
                                    <button type="button" className="btn btn-success"
                                            onClick={redirectForm}>{t("content.on-success.create-new-test")}
                                    </button>
                                </div>
                                <div className="col-sm-6">
                                    <Link to="/home">
                                        <button type="button"
                                                className="btn btn-primary">{t("content.on-success.go-home")}
                                        </button>
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div> :
                    <div>
                        <div className="row g-5">
                            <div className="col-md-5 col-lg-4">
                                <h4 className="d-flex justify-content-between align-items-center mb-3">
                                    <span className="text-primary">{t("content.test-info")}</span>
                                </h4>
                            </div>
                            <div className="col-md-7 col-lg-8 order-md-last">
                                <TestInfoForm testInfo={testInfo} handleTestInfo={handleTestInfo}/>
                            </div>
                        </div>
                        <hr/>
                        <div className="row g-5">
                            <div className="col-md-5 col-lg-4">
                                <h4 className="d-flex justify-content-between align-items-center mb-3">
                                    <span className="text-primary">{t("content.add-question")}</span>
                                </h4>
                                {/* Essay Question */}
                                <div className="col-12">
                                    <QuestionModalAndButton
                                        questionType="essay"
                                        questionList={questionList}
                                        addNewQuestion={handleNewQuestions}
                                        questionNumber={questionNumber}
                                        incrementQuestionNumber={incrementQuestionNumber}/>
                                </div>
                                {/* Alternative Question */}
                                <div className="col-12 mt-2">
                                    <QuestionModalAndButton
                                        questionType="mc"
                                        questionList={questionList}
                                        addNewQuestion={handleNewQuestions}
                                        questionNumber={questionNumber}
                                        incrementQuestionNumber={incrementQuestionNumber}/>
                                </div>
                                {/* Tof Question */}
                                <div className="col-12 mt-2">
                                    <QuestionModalAndButton
                                        questionType="tof"
                                        questionList={questionList}
                                        addNewQuestion={handleNewQuestions}
                                        questionNumber={questionNumber}
                                        incrementQuestionNumber={incrementQuestionNumber}/>
                                </div>
                            </div>
                            <div id="question-list-container" className="col-md-7 col-lg-8 order-md-last">
                                {testInfo["question_list"] && testInfo["question_list"].length > 0 && testInfo["question_list"].map(question => (
                                    <div className="card mb-3" key={`question-${question.number}`}>
                                        <div className="card-header text-end">
                                            <button type="button" className="btn-close" aria-label="Close"
                                                    onClick={(event) => {
                                                        deleteQuestion(event, question)
                                                    }}/>
                                        </div>
                                        <div className="card-body">
                                            <h5 className="card-title">{`${question.number + 1}) ${question.title}`}</h5>
                                            <p className="card-text">{question['formulation']}</p>
                                        </div>
                                        <div className="card-img-container align-content-center d-flex overflow-auto">
                                            {question["image_list"] && question["image_list"].length > 0 && question["image_list"].map(image => (
                                                <div className="img-thumbnail-container"
                                                     key={`question-${question.number}-img-${image.number}`}>
                                                    <img id={`question-${question.number}-img-${image.number}`}
                                                         className="thumbnail-preview"
                                                         src={URL.createObjectURL(image.file)}
                                                         alt={image.name}/>
                                                </div>
                                            ))}
                                        </div>
                                        {question.type === 'mc' && (
                                            <div>
                                                <ol className="alternative-list-preview" type="A">
                                                    <li>{question["alternative_a"]}</li>
                                                    <li>{question["alternative_b"]}</li>
                                                    <li>{question["alternative_c"]}</li>
                                                    <li>{question["alternative_d"]}</li>
                                                    <li>{question["alternative_e"]}</li>
                                                </ol>
                                            </div>
                                        )}
                                    </div>
                                ))}
                            </div>
                        </div>
                        <hr/>
                        <div className="row justify-content-center">
                            <div className="col-md-auto mb-3">
                                {testInfo.question_list && testInfo.question_list.length > 0 &&
                                <div>
                                    <button id="create-test-button"
                                            className="btn btn-success"
                                            data-bs-toggle="modal"
                                            data-bs-target="#previewModal"
                                            onClick={previewTest}>{t("content.preview")}
                                    </button>

                                    <div className="modal fade" id="previewModal" data-bs-backdrop="static"
                                         data-bs-keyboard="false" tabIndex="-1" aria-labelledby="previewModalLabel"
                                         aria-hidden="true">
                                        <div className="modal-dialog modal-xl">
                                            <div className="modal-content">
                                                <div className="modal-body">
                                                    <TestPreview testInfo={testInfo}/>
                                                </div>
                                                <div className="modal-footer">
                                                    <button type="button" className="btn btn-secondary"
                                                            data-bs-dismiss="modal" onClick={() => {
                                                        let submitTestPreviewButton = document.getElementById('submit-test-preview');
                                                        if (submitTestPreviewButton) {
                                                            submitTestPreviewButton.click();
                                                        }
                                                    }}>{t("content.alert-modal.close")}
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                }
                            </div>
                            <div className="col-md-auto mb-3">
                                <button type="button" className="btn btn-primary" onClick={askForSubmit}>
                                    {t("content.create-test")}
                                </button>

                                <button id="accept-modal-btn" type="button" className="btn btn-primary"
                                        data-bs-toggle="modal"
                                        data-bs-target="#acceptModal" hidden/>

                                <div className="modal fade" id="acceptModal" data-bs-backdrop="static"
                                     data-bs-keyboard="false"
                                     tabIndex="-1" aria-labelledby="acceptModalLabel" aria-hidden="true">
                                    <div className="modal-dialog">
                                        <div className="modal-content">
                                            <div className="modal-body">
                                                {t("content.alert-modal.test-creation-message")}
                                            </div>
                                            <div className="modal-footer">
                                                <button id="close-accept-modal-btn" type="button"
                                                        className="btn btn-secondary"
                                                        data-bs-dismiss="modal">{t("content.alert-modal.close")}
                                                </button>
                                                <button type="button" className="btn btn-primary"
                                                        onClick={onSubmit}>{t("content.alert-modal.accept")}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-auto mb-3">
                                <button type="button" className="btn btn-danger" data-bs-toggle="modal"
                                        data-bs-target="#cancelModal">
                                    {t("content.cancel")}
                                </button>

                                <div className="modal fade" id="cancelModal" data-bs-backdrop="static"
                                     data-bs-keyboard="false" tabIndex="-1" aria-labelledby="cancelModalLabel"
                                     aria-hidden="true">
                                    <div className="modal-dialog">
                                        <div className="modal-content">
                                            <div className="modal-body">
                                                {t("content.alert-modal.cancel-message")}
                                            </div>
                                            <div className="modal-footer">
                                                <button type="button" className="btn btn-secondary"
                                                        data-bs-dismiss="modal">{t("content.alert-modal.close")}
                                                </button>
                                                <Link to="/home">
                                                    <button type="button" className="btn btn-primary"
                                                            data-bs-dismiss="modal">{t("content.alert-modal.accept")}
                                                    </button>
                                                </Link>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                }
            </div>
        </div>
    );
}