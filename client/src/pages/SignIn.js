import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {useTranslation} from 'react-i18next';
import {Link, Redirect} from 'react-router-dom';

import {login} from '../actions/auth';

import ewe_logo from '../assets/ewe_logo.png';
import {clearMessage} from '../actions/message';

const SignIn = (props) => {
    const [t, i18n] = useTranslation("global");

    const [signInFormData, setSignInFormData] = useState({});

    const {isLoggedIn} = useSelector(state => state.auth);
    const {message, message_t_code} = useSelector(state => state.message);

    console.log(message_t_code)

    const dispatch = useDispatch();

    useEffect(() => {
        props.history.listen((location) => {
            dispatch(clearMessage()); // clear message when changing location
        });
    }, []);

    const handleInputChange = (event) => {
        const target = event.target;
        let name = target.name;
        setSignInFormData(testInfo => ({...testInfo, [name]: target.value}));
    };

    const changeLanguage = (lang) => {
        i18n.changeLanguage(lang)
            .then(r => localStorage.setItem('lang', lang));

    };

    const handleKeyEnter = (event) => {
        if (event.key === 'Enter') {
            handleSubmit(event)
        }
    };

    const handleSubmit = (event) => {
        event.preventDefault();
        console.log(signInFormData);
        let form = document.getElementById(`sign-in-form`);
        if (!form.checkValidity()) {
            form.reportValidity();
        } else {
            console.log('Success');
            dispatch(login(signInFormData))
                .then(() => {
                    props.history.push("/home");
                    window.location.reload();
                })
                .catch((e) => {
                    console.log(e);
                });
        }
    };

    if (isLoggedIn) {
        return <Redirect to="/home"/>;
    }

    return (
        <div className="text-center">
            <div className="form-signin">
                <form id="sign-in-form" action="/">
                    <img className="mb-4" src={ewe_logo} alt="ewe_logo" width="192" height="108"/>
                    <h1 className="h3 mb-3 fw-normal">{t("signin.title")}</h1>

                    <div className="form-floating">
                        <input type="email"
                               id="floatingInput"
                               className="form-control"
                               placeholder="name@example.com"
                               name="email"
                               value={signInFormData.email}
                               onChange={handleInputChange}
                               required/>
                        <label htmlFor="floatingInput">{t("signin.email-label")}</label>
                    </div>

                    <div className="form-floating">
                        <input type="password"
                               id="floatingPassword"
                               className="form-control"
                               placeholder="password"
                               name="password"
                               value={signInFormData.password}
                               onChange={handleInputChange}
                               onKeyUp={handleKeyEnter}
                               required/>
                        <label htmlFor="floatingPassword">{t("signin.password-label")}</label>
                    </div>

                    <div className="checkbox mb-3">
                        <label>
                            <input type="checkbox" value="remember-me"/> {t("signin.remember-me")}
                        </label>
                    </div>

                    <div className="mb-3">
                        <Link to="/signup">{t("signin.signup-link")}</Link>
                    </div>

                    <button className="w-100 btn btn-lg btn-primary mb-3" type="button"
                            onClick={handleSubmit}>{t("signin.signin-button")}</button>

                    {message_t_code && (
                        <div id="signup-messages" className="text-center">
                            <div className="error-container">
                                {t(message_t_code)}
                            </div>
                        </div>
                    )}
                </form>
            </div>
            <div className="container">
                <nav aria-label="breadcrumb">
                    <ol className="breadcrumb">
                        <li className="breadcrumb-item"><span onClick={() => changeLanguage("es")}>{t("languages.spanish")}</span></li>
                        <li className="breadcrumb-item"><span onClick={() => changeLanguage("en")}>{t("languages.english")}</span></li>
                        <li className="breadcrumb-item"><span onClick={() => changeLanguage("fr")}>{t("languages.french")}</span></li>
                    </ol>
                </nav>
            </div>
        </div>
    );
};

export default SignIn;