import React, {useEffect, useRef, useState} from 'react';
import {Redirect, useParams} from 'react-router-dom';
import {useTranslation} from 'react-i18next';

import '../App.css';
import Navbar from '../components/Navbar';

import {useSelector} from 'react-redux';
import axios from 'axios';
import authHeader from '../services/auth-header';

const {API_URL} = require('../common/Constants');


export default function TestAnswering() {
    const [t] = useTranslation("test_answering");
    const leftTimeInitialMount = useRef(true);
    const currPageInitialMount = useRef(true);
    const {testId} = useParams();

    const {user: currentUser} = useSelector((state) => state.auth);
    const [testInfo, setTestInfo] = useState();
    const [instructionsRead, setInstructionsRead] = useState(false);
    const [errorMessage, setErrorMessage] = useState();
    const [currPage, setCurrPage] = useState(0);
    const [lastPage, setLastPage] = useState();
    const [leftTime, setLeftTime] = useState(0);
    const firstPage = 0;
    const [paperSheet, setPaperSheet] = useState({});
    const [finished, setFinished] = useState(false);
    const [testFullyAnswered, setTestFullyAnswered] = useState(false);

    let date = new Date(null);
    date.setSeconds(leftTime); // specify value for SECONDS here
    let leftTimeString = date.toISOString().substr(11, 8);

    useEffect(() => {
        if (currPageInitialMount.current) {
            currPageInitialMount.current = false;
        } else {
            localStorage.setItem('current-page', currPage.toString());
        }
    }, [currPage]);

    useEffect(() => {
        const answers = JSON.parse(localStorage.getItem('answers'));
        const savedPage = localStorage.getItem('current-page');
        if (savedPage) {
            setCurrPage(parseInt(savedPage));
        }
        axios.get(API_URL + 'test/testid/' + testId, {headers: authHeader()})
            .then(response => {
                if (response.data && response.data.data) {
                    setTestInfo(response.data.data);
                    if (answers) {
                        Object.keys(answers).forEach(i => {
                            response.data.data.question_list[i].answer = answers[i].answer;
                            if (answers[i].answer && (answers[i].answer !== "" || answers[i].answer === 0)) {
                                response.data.data.question_list[i].answered = true;
                            }
                            if (answers[i].justification) {
                                response.data.data.question_list[i].justification = answers[i].justification;
                            }
                        })
                    }
                    setPaperSheet(paperSheet => ({...paperSheet, ["questions"]: response.data.data.question_list}))
                    setLastPage(response.data.data.question_list.length - 1);
                    axios.get(API_URL + 'answer/papersheet/testid/' + testId, {headers: authHeader()})
                        .then(psResponse => {
                            if (psResponse.data && psResponse.data.data) {
                                if (psResponse.data.data.submission_date) {
                                    setFinished(true);
                                }
                                setPaperSheet(paperSheet => ({
                                    ...paperSheet,
                                    ["start_date"]: psResponse.data.data.start_date
                                }))
                                const timeNow = new Date();
                                const diff = Math.round((timeNow - new Date(psResponse.data.data.start_date)) / 1000);
                                setLeftTime(response.data.data.duration - diff);
                                setInstructionsRead(true);
                            }
                        })
                }
            });
    }, []);


    useEffect(() => {
        if (leftTimeInitialMount.current) {
            leftTimeInitialMount.current = false;
        } else {
            if (leftTime >= 0) {
                const tmp = setInterval(() => {
                    const timeNow = new Date();
                    const diff = Math.round((timeNow - new Date(paperSheet.start_date)) / 1000);
                    setLeftTime(testInfo.duration - diff);
                }, 1000)
                return () => {
                    clearInterval(tmp);
                };
            } else {
                setLeftTime(-1);
            }
        }
    }, [leftTime]);


    const beginTest = () => {
        axios.post(API_URL + 'answer/papersheet/new', {testId}, {headers: authHeader()})
            .then(response => {
                setInstructionsRead(true);
                setPaperSheet(paperSheet => ({...paperSheet, ["start_date"]: new Date()}))
                setLeftTime(testInfo.duration);
            }).catch(e => {
            console.log(e)
            setErrorMessage(e.response.data.message_t_code)
        })
    }

    const prevPage = () => {
        if (currPage > firstPage) {
            setCurrPage(currPage - 1);
        }
    }

    const nextPage = () => {
        if (currPage < lastPage) {
            setCurrPage(currPage + 1);
        }
    }

    const answer = (event) => {
        event.preventDefault();
        const answerObj = localStorage.getItem('answers') ? JSON.parse(localStorage.getItem('answers')) : {};
        const currAnswer = paperSheet.questions[currPage].answer;
        if (currAnswer || currAnswer === 0) {
            answerObj[currPage] = {
                "answer": currAnswer,
                "justification": paperSheet.questions[currPage].justification
            };
            localStorage.setItem('answers', JSON.stringify(answerObj));
            paperSheet.questions[currPage].answered = true;
        } else {
            console.log('ANSWER NEEDED', currAnswer);
            delete answerObj[currPage];
            localStorage.setItem('answers', JSON.stringify(answerObj));
            paperSheet.questions[currPage].answered = false;
        }
        checkTestIsFullyAnswered();
    }

    const handleAnswer = (event) => {
        const target = event.target;
        const name = target.name;
        const paperBuffer = {...paperSheet};
        if (name === `justification-${currPage}`) {
            paperBuffer.questions[currPage].justification = target.value;
        } else if (event.target.name === `radio-q${currPage}`) {
            paperBuffer.questions[currPage].answer = parseInt(target.id.split('-a')[1]);
        } else {
            paperBuffer.questions[currPage].answer = target.value;
        }
        setPaperSheet(paperBuffer);
    }

    const checkTestIsFullyAnswered = () => {
        const answers = localStorage.getItem('answers') ? JSON.parse(localStorage.getItem('answers')) : {};
        if (Object.keys(answers).length === paperSheet.questions.length) {
            setTestFullyAnswered(true);
        } else {
            setTestFullyAnswered(false);
        }
    }

    const submitTest = () => {
        paperSheet.testId = testId;
        paperSheet.course = testInfo.course;
        document.getElementById('close-submission-modal').click();
        axios.post(API_URL + 'answer/test-submission', paperSheet, {headers: authHeader()})
            .then(response => {
                localStorage.removeItem('answers');
                localStorage.removeItem('current-page');
                console.log(response);
                setFinished(true);
            }).catch(e => {
            console.log(e.response);
        });
    }

    if (!currentUser) {
        return <Redirect to="/login"/>;
    }

    if (leftTime < 0) {
        if (!finished) {
            submitTest();
        }
        return <Redirect to="/"/>;
    }

    if (finished) {
        return <Redirect to="/"/>;
    }

    return (
        <div>
            <Navbar/>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-2">
                        {instructionsRead &&
                        <div>
                            <div className="mt-3">
                                <button type="button" className="btn btn-primary" data-bs-toggle="modal"
                                        data-bs-target="#instructions-modal">
                                    {t("left-sidebar.show-instructions")}
                                </button>
                            </div>
                            <div className="modal fade" id="instructions-modal" tabIndex="-1"
                                 aria-labelledby="instructions-modal-label" aria-hidden="true">
                                <div className="modal-dialog modal-dialog-centered">
                                    <div className="modal-content">
                                        <div className="modal-header">
                                            <h5 className="modal-title" id="instructions-modal-label">{t("left-sidebar.instructions")}</h5>
                                        </div>
                                        <div className="modal-body">
                                            {testInfo && testInfo.instructions}
                                        </div>
                                        <div className="modal-footer">
                                            <button type="button" className="btn btn-secondary"
                                                    data-bs-dismiss="modal">{t("left-sidebar.close")}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        }
                    </div>
                    <div className="col-8">
                        {testInfo &&
                        <div>
                            {!instructionsRead ?
                                <div className="text-center mt-3">
                                    {errorMessage && (
                                        <div id="signup-messages" className="text-center">
                                            <div className="error-container">
                                                {t(errorMessage)}
                                            </div>
                                        </div>
                                    )}
                                    <div className="modal-content mt-3">
                                        <div className="card">
                                            <div className="card-body">
                                                <h5 className="card-title">{testInfo.title}</h5>
                                                <p className="card-text">{testInfo.instructions}</p>
                                            </div>
                                        </div>

                                        <div className="container mt-3 mb-3">
                                            <button type="button" className="btn btn-primary"
                                                    onClick={beginTest}>{t("content.begin-test")}</button>
                                        </div>
                                    </div>
                                </div> :
                                <div>
                                    <div className="modal-content mt-3">
                                        <div className="card">
                                            <div className="card-body">
                                                <h5 className="card-title">{`${currPage + 1}) ${testInfo.question_list[currPage].title}`}</h5>
                                                <p className="card-text">{testInfo.question_list[currPage].detailed.formulation}</p>
                                            </div>
                                            <div className="card-img-container align-content-center text-center">
                                                {
                                                    testInfo.question_list[currPage].detailed.imgFiles &&
                                                    testInfo.question_list[currPage].detailed.imgFiles.map((file, index) => (
                                                        <img id={'img-' + currPage + ' - ' + index}
                                                             src={API_URL + "test/testimg/" + file}
                                                             style={{height: "150px", width: "270px"}}
                                                             className="card-img-bottom img-preview-wrapper"
                                                             alt={'img-' + currPage + ' - ' + index}/>
                                                    ))
                                                }
                                            </div>
                                        </div>


                                        <div className="container mt-3 mb-3">
                                            <form id={`testInfoForm-q${currPage}`} className="needs-validation"
                                                  noValidate>
                                                {testInfo.question_list[currPage].question_type === 1 &&
                                                <div className="row g-3">
                                                    <div className="col-12">
                                                        <label htmlFor="text-area">{t("content.answer")}</label>
                                                        <div className="form-floating">
                                                            <textarea className="form-control" id="text-area"
                                                                      name={`res-${currPage}`}
                                                                      value={paperSheet.questions[currPage].answer || ''}
                                                                      style={{height: "120px"}}
                                                                      onChange={handleAnswer}/>
                                                        </div>
                                                    </div>

                                                    {/*<div className="col-12">*/}
                                                    {/*    <label htmlFor="formFileSm"*/}
                                                    {/*           className="form-label">{t("content.upload-image")}*/}
                                                    {/*        <span> ({t("content.optional")})</span></label>*/}
                                                    {/*    <input className="form-control form-control-sm" id="formFileSm"*/}
                                                    {/*           type="file"/>*/}
                                                    {/*    <div className="invalid-feedback">*/}
                                                    {/*        Valid first name is required.*/}
                                                    {/*    </div>*/}
                                                    {/*</div>*/}
                                                </div>
                                                }

                                                {testInfo.question_list[currPage].question_type === 2 &&
                                                <div className="row g-3">
                                                    <div className="col-12">
                                                        <label htmlFor="text-area">{t("content.answer")}</label>
                                                        <div className="form-floating">
                                                            {paperSheet.questions[currPage].detailed.alternatives.map((alternative, index) => (
                                                                <div className="form-check">
                                                                    {index === paperSheet.questions[currPage].answer ?
                                                                        <input type="radio" className="form-check-input"
                                                                               name={`radio-q${currPage}`}
                                                                               id={`radio-q${currPage}-a${index}`}
                                                                               onChange={handleAnswer} checked/> :
                                                                        <input type="radio" className="form-check-input"
                                                                               name={`radio-q${currPage}`}
                                                                               id={`radio-q${currPage}-a${index}`}
                                                                               onChange={handleAnswer}/>
                                                                    }
                                                                    <label htmlFor={`radio-q${currPage}-a${index}`}
                                                                           className="form-check-label">
                                                                        {alternative}
                                                                    </label>
                                                                </div>
                                                            ))}
                                                        </div>
                                                    </div>
                                                </div>
                                                }

                                                {testInfo.question_list[currPage].question_type === 3 &&
                                                <div className="row g-3">
                                                    <div className="col-12">
                                                        <label
                                                            htmlFor={`tof-q${currPage}`}>{t("content.answer")}</label>
                                                        <div className="form-floating">
                                                            <select id={`tof-q${currPage}`}
                                                                    name={`res-${currPage}`}
                                                                    value={paperSheet.questions[currPage].answer || ''}
                                                                    onChange={handleAnswer}>
                                                                <option
                                                                    hidden
                                                                    value="">{t("content.tof-question.select-option")}</option>
                                                                <option
                                                                    value="T">{t("content.tof-question.true")}</option>
                                                                <option
                                                                    value="F">{t("content.tof-question.false")}</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div className="col-12">
                                                        <label
                                                            htmlFor={`justification-q${currPage}`}>{t("content.tof-question.justification")}</label>
                                                        <div className="form-floating">
                                                            <textarea className="form-control"
                                                                      id={`justification-q${currPage}`}
                                                                      name={`justification-${currPage}`}
                                                                      value={paperSheet.questions[currPage].justification || ''}
                                                                      style={{height: "120px"}}
                                                                      onChange={handleAnswer}/>
                                                        </div>
                                                    </div>
                                                </div>
                                                }

                                                <button type="button"
                                                        className="btn btn-primary mt-3"
                                                        onClick={(event) => {
                                                            answer(event)
                                                        }}>{t("content.answer-question")}</button>
                                            </form>
                                        </div>
                                    </div>

                                    <nav className="mt-3" aria-label="Page navigation example">
                                        <ul className="pagination justify-content-center">
                                            <li className={currPage === firstPage ? "page-item disabled" : "page-item"}>
                                                <button className="page-link" tabIndex="-1"
                                                        onClick={prevPage}>{t("pagination.previous")}</button>
                                            </li>
                                            {testInfo.question_list.map((question, index) => (
                                                <li className={currPage === index ? "page-item active" : "page-item"}>
                                                    <button className="page-link"
                                                            onClick={() => setCurrPage(index)}>{index + 1}</button>
                                                </li>
                                            ))}
                                            <li className={currPage === lastPage ? "page-item disabled" : "page-item"}>
                                                <button className="page-link"
                                                        onClick={nextPage}>{t("pagination.next")}</button>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                            }
                        </div>
                        }
                    </div>
                    <div className="col-2">
                        {instructionsRead &&
                        <div>
                            <div className="card mt-3">
                                <div className="card-body">
                                    <h5 className="card-title">{t("right-sidebar.time-left")}</h5>
                                    <h6 className="card-subtitle mb-2 text-muted">{t("right-sidebar.time-left-message")}</h6>
                                    <h2 className="card-text">{leftTimeString}</h2>
                                </div>
                            </div>
                            <div className="card mt-3">
                                <div className="card-body">
                                    <h5 className="card-title">{t("right-sidebar.answered-questions")}</h5>
                                    <div className="flex-row">
                                        {paperSheet.questions && paperSheet.questions.map((question, index) => (
                                            <span
                                                className={"question-number-list-grid " + (question.answered ? " answered " : "not-answered ") + (index === currPage ? "current" : "")}>{index + 1}</span>
                                        ))}
                                    </div>
                                    <div className="row d-flex">
                                        <div className="col-2">
                                            <span className="legend-dot not-answered"/>
                                        </div>

                                        <div className="col-10">
                                            {t("right-sidebar.answered-questions-labels.not-answered")}
                                        </div>
                                    </div>

                                    <div className="row d-flex">
                                        <div className="col-2">
                                            <span className="legend-dot current"/>
                                        </div>

                                        <div className="col-10">
                                            {t("right-sidebar.answered-questions-labels.current")}
                                        </div>
                                    </div>

                                    <div className="row d-flex">
                                        <div className="col-2">
                                            <span className="legend-dot answered"/>
                                        </div>

                                        <div className="col-10">
                                            {t("right-sidebar.answered-questions-labels.answered")}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="mt-3">
                                <button type="button" className="btn btn-primary" data-bs-toggle="modal"
                                        data-bs-target="#test-submission-modal" onClick={checkTestIsFullyAnswered}>
                                    {t("right-sidebar.submit-test")}
                                </button>
                            </div>
                            <div className="modal fade" id="test-submission-modal" tabIndex="-1"
                                 aria-labelledby="test-submission-modal-label" aria-hidden="true">
                                <div className="modal-dialog modal-dialog-centered">
                                    <div className="modal-content">
                                        <div className="modal-header">
                                            <h5 className="modal-title" id="test-submission-modal-label">{t("content.submission-modal.warning")}</h5>
                                        </div>
                                        <div className="modal-body">
                                            {t("content.submission-modal.warning-message")}
                                            {!testFullyAnswered &&
                                            <div>
                                                <hr/>
                                                <span className="text-danger">* {t("content.submission-modal.test-not-full")}</span>
                                            </div>
                                            }
                                        </div>
                                        <div className="modal-footer">
                                            <button id="close-submission-modal" type="button"
                                                    className="btn btn-secondary"
                                                    data-bs-dismiss="modal">{t("content.submission-modal.close")}
                                            </button>
                                            <button type="button" className="btn btn-primary"
                                                    onClick={submitTest}>{t("content.submission-modal.submit")}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        }
                    </div>
                </div>
            </div>
        </div>
    );
}