import React from 'react';

const NotFound404 = () => {
    return (
        <div>
            <div className="container text-center">
                <div className="row">
                    <div className="col-12">
                        <div className="not-found-logo mt-4">
                            <h1 className="h-100"> 404 Not Found</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default NotFound404;