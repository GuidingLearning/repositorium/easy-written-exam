import React from 'react';
import Navbar from '../components/Navbar';
import CourseList from '../components/CourseList';

const Courses = () => {
    return (
        <div>
            <Navbar/>
            <CourseList/>
        </div>
    );
};

export default Courses;