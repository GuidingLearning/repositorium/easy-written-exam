import React from 'react';
import {useSelector} from 'react-redux';
import {useTranslation} from 'react-i18next';

import JoinCourseForm from '../components/JoinCourseForm';
import Navbar from '../components/Navbar';

const JoinCourse = () => {
    const [t] = useTranslation("courses");

    const {user: currentUser} = useSelector((state) => state.auth);

    return (
        <div>
            <Navbar/>
            <div id="header-title" className="py-5 text-center">
                <h2>{t("join.page-title")}</h2>
            </div>
            <JoinCourseForm userId={currentUser.id}/>
        </div>
    );
};

export default JoinCourse;