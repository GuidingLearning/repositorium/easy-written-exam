const SERVER_URL = 'https://easywrittenexam.repositorium.cl/api/';
const LOCAL_URL = 'http://ewe.local:3101/api/';
const DEV_URL = 'http://localhost:3101/api/';

export const API_URL = DEV_URL;
