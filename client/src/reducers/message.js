import { SET_MESSAGE, CLEAR_MESSAGE } from '../actions/types';

const initialState = {};

export default function (state = initialState, action) {
    const { type, payload } = action;

    switch (type) {
        case SET_MESSAGE:
            return {
                message: payload.message,
                message_t_code: payload.message_t_code
            };

        case CLEAR_MESSAGE:
            return {
                message: "",
                message_t_code: null
            };

        default:
            return state;
    }
}