import axios from 'axios';
import authHeader from './auth-header';

const {API_URL} = require('../common/Constants');

const getPublicContent = () => {
    return axios.get(API_URL + 'test/all');
};

const getUserBoard = () => {
    return axios.get(API_URL + 'test/user', {headers: authHeader()});
};

const getModeratorBoard = () => {
    return axios.get(API_URL + 'test/mod', {headers: authHeader()});
};

const getAdminBoard = () => {
    return axios.get(API_URL + 'test/admin', {headers: authHeader()});
};

export default {
    getPublicContent,
    getUserBoard,
    getModeratorBoard,
    getAdminBoard,
};