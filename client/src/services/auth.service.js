import axios from 'axios';

const {API_URL} = require('../common/Constants');

const register = (formData) => {
    return axios.post(API_URL + 'auth/signup', formData);
};

const login = (formData) => {
    return axios
        .post(API_URL + 'auth/signin', formData)
        .then((response) => {
            if (response.data.accessToken) {
                localStorage.setItem('user', JSON.stringify(response.data));
            }
            return response.data;
        });
};

const logout = () => {
    localStorage.removeItem('user');
};

export default {
    register,
    login,
    logout,
};